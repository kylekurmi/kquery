import kQuery from './executeKurmiAPI.js'
export default {
    props: {
      linkComponentType: String,
      item: Object,
      modelValue: Object,
      workspace: String
    },
    emits: ['update:modelValue'],
    created() {

    },
    computed:{
        async referencedRule() {
            let ruleContent = (await kQuery.getWorkspaceFile(this.workspace,this.item.referencedTemplateId)).content;
            return JSON.parse(ruleContent);
        }
    },
    data() {
        return {
            
            availableRules: [],
            referencedTemplateId: this.item.referencedTemplateId || "",
            bindings: this.item.bindings || [],
            linkInfo: this.item.linkInfo || {},
            javascriptCondition: this.item.javascriptCondition
        }
    },
    methods: {
        bindingVariableNames(varName){
          return ["*",varName];
      }
    },
    watch: {
        javascriptCondition(newValue,oldValue) {
            this.$emit('update:modelValue', {bindings: this.bindings,linkInfo: this.linkInfo,javascriptCondition: this.javascriptCondition, referencedTemplateId: this.referencedTemplateId });
        },
        bindings:{
            handler(newValue,oldValue){
                
                this.$emit('update:modelValue', {bindings: this.bindings,linkInfo: this.linkInfo,javascriptCondition: this.javascriptCondition, referencedTemplateId: this.referencedTemplateId });
            },
            deep: true
        },
        referencedTemplateId(val){
            this.bindings.splice(0,this.bindings.length,{variableName:"arrayOfSomething"});
            this.$emit('update:modelValue', {bindings: this.bindings,linkInfo: this.linkInfo,javascriptCondition: this.javascriptCondition, referencedTemplateId: this.referencedTemplateId });
            
        }
    },
    template: `
    <section :title="JSON.stringify(item,null,'\t')" class="rule-field-linkitem">
        <!--<label>{{linkComponentType}} <select v-model.lazy="referencedTemplateId"><option v-for="templateId in availableRules">{{templateId}}</option></select></label>-->
        <h3>{{referencedTemplateId}}</h3>
        <label  v-for="binding in bindings"> {{binding.referencedVariableName}} 
            <select v-model.lazy="binding.variableName">
                <option v-for="refVar in bindingVariableNames(binding.referencedVariableName)">{{refVar}}</option>
            </select>
        </label>
        <label>javascriptCondition <textarea v-model.lazy="javascriptCondition"></textarea></label>
    </section>
    `
  }
  
  