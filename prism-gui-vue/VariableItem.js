import FormItem from './FormItem.js'

export default {
  name: 'VariableItem',
  components: { FormItem },
  props: {
    variableData: Object
  },
  computed: {
    
  },
  data() {
    return {
      jsonPath: "",
      variableName: "",
      javascriptCondition: "",
      childs: []
    }
  },
  created() {
      var $this = this;
      Object.entries($this.variableData).forEach(function([key,value]){
        $this[key] = value;
      });
  },
  watch: {
    
    /*"fieldObject.expression"(val){
      $.emit('change-expression', val);
    }*/

  },
  template: `
  <section>
    <form-item v-model="jsonPath" :name="'jsonPath'" :field-type="'string'" :answer="JSON.stringify(jsonPath)" ></form-item>
    <form-item v-model="variableName" :name="'variableName'" :field-type="'string'" :answer="JSON.stringify(variableName)"></form-item>
    <form-item v-model="javascriptCondition" :name="'javascriptCondition'" :field-type="'textarea'" :answer="JSON.stringify(javascriptCondition)"></form-item>
    <ul>
      <li :key="ch.jsonPath" v-for="ch in childs"><variable-item  :variable-data="ch"></variable-item></li>
    </ul>
  </section>
  `
}

