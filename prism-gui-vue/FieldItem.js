import LinkItem from './LinkItem.js'
export default {
  components: { LinkItem },
  props: {
    name: String,
    fieldType: String,
    fieldObject: Object,
    modelValue: Object,
    workspace: String,
    availableRules: Array

  },
  data(){
    return {fieldObjectData: JSON.parse(JSON.stringify(this.fieldObject))}

  },
  emits: ['update:modelValue'],
  methods: {
    removeLinkItem(pos){
      console.debug("linkItem Array",this.fieldObjectData.linkItem);
      this.fieldObjectData.linkItem.splice(pos,1);
      console.debug("linkItem Array",this.fieldObjectData.linkItem);
    }
  },
  computed: {
    inputType(){
      if (this.fieldType == "boolean"){
        return "checkbox"
      }
    }
  },
  created() {
    if (this.fieldObjectData.linkItem){
      //this.fieldObject.linkItem.push({});
    }
    
  },
  watch: {
    fieldObjectData:{
      handler(newValue, oldValue) {
        this.$emit('update:modelValue',newValue);
      },
      deep: true
      
    }
    /*"fieldObject.expression"(val){
      $.emit('change-expression', val);
    }*/

  },
  template: `
  <label class="rule-field-item"><span>{{name}}</span>
    <textarea v-if="fieldObjectData.expression != undefined" v-model.lazy="fieldObjectData.expression"></textarea>
    <section class="rule-links" v-if="fieldObjectData.linkItem">
      <ol >
        <li v-for="(litem,litemIndx) in fieldObjectData.linkItem" :key="litemIndx">
        <button @click="removeLinkItem(litemIndx)">Delete Item Pos {{litem.referencedTemplateId}}</button>
          <link-item 
          :linkedComponentType="name"
          :availableRules=[litem.referencedTemplateId]
          :item="litem"
          v-model.lazy="fieldObjectData.linkItem[litemIndx]"
          :workspace="workspace"      
        ></link-item>
        </li>
      </ol>
      <select @change="fieldObjectData.linkItem.push({referencedTemplateId:$event.target.value}); $event.target.value = '';" ><option value="">New link to {{name}}</option><option v-for="templateId in availableRules">{{templateId}}</option></select>
    </section>
  </label>
  `
}

