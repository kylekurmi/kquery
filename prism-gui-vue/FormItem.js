export default {
  props: ['name','fieldType','enums','answer','multiple','modelValue'],
  xprops: {
    name: String,
    fieldType: String,
    enums: Array,
    answer: String,
    multiple: Boolean,
    modelValue: String
  },
  emits: ['update:modelValue'],
  data(){
    return {
      parsedAnswer: JSON.parse(this.answer)
    }
  },
  computed: {
    enumlist() {
      return this.enums.map(function(_enum) { return {value: _enum.value || _enum, text: _enum.text || _enum};   } );
    },
    inputType(){
      if (this.fieldType == "boolean"){
        return "checkbox";
      }
      if (this.fieldType == "password"){
        return "password";
      }
    }
  },
  watch: {
    parsedAnswer(val){
      this.$emit('update:modelValue', val);
    }

  },
  template: `
  <label><span>{{name}}               </span>
    <textarea v-if="fieldType == 'textarea'" v-model="parsedAnswer"></textarea>
    <input v-else-if="!enums" :type="inputType" v-model="parsedAnswer"/>
    <select :multiple="multiple" v-else v-model="parsedAnswer">
      <option :value="_enum.value" v-for="_enum in enumlist">
        {{_enum.text}}
      </option>
    </select>
  </label>
  `
}

