import TodoItem from './TodoItem.js'
import FormItem from './FormItem.js'
import FieldItem from './FieldItem.js'
import LinkItem from './LinkItem.js'
import VariableItem from './VariableItem.js'
import kQuery from './executeKurmiAPI.js';

async function getRule(workspace, path){
  let getWorkspaceFileResult = await kQuery.getWorkspaceFile(workspace,path)
  let ruleContent = getWorkspaceFileResult && getWorkspaceFileResult.content;
  return ruleContent && JSON.parse(ruleContent);
}

export default {
  components: {
    TodoItem,FormItem,FieldItem,LinkItem,VariableItem

  },
  props: {
    path: String,
    workspace: String,
    availableRules: Array
  },
  data() {
    return {
      ruleSchema: {
        "templateId": { "title": "templateId"},
        "componentType": { title: "componentType" },
        "creationMode": { title: "creationMode", enums: ["MAY_EXISTS", "MUST_EXISTS", "CREATION"] },
        "allowUpdate": { title: "allowUpdate", fieldType: "boolean" },
        "allowDelete": { title: "allowDelete", fieldType: "boolean" },
        "useDefaultIdentifier": { title: "useDefaultIdentifier", fieldType: "boolean" },
        "matchOn": { title: "matchOn", fieldType: "array", multiple: true  }
      },
      enumDictionary: {
        creationMode: ["MAY_EXISTS","MUST_EXISTS","CREATE"]
      },
      rule : null,
      sourceView: false
    }
  },
  watch: {
    path(val){
      if (val){
        var $this = this;
        getRule(this.workspace,this.path).then(r=>  {$this.rule = r; });
      }
    },
    rule(val){
      this.enumDictionary["matchOn"] = Object.keys(val.fields);
    }

  },
  computed: {
    
  },
  methods: {
    formItemChange() {
      //$.emit('change-form');
    }
  },
  created() {
    var $this = this;
    getRule(this.workspace,this.path).then(r=>{$this.rule = r; });
  },
  template: `
  <section class="rule-item" v-if="rule">
  <h1>{{rule.templateId}}</h1>
    <label>Source View <input type="checkbox" v-model="sourceView"/></label>
    <textarea class="rule-item-source" v-if="sourceView" @change.lazy="this.rule = JSON.parse($event.target.value)">{{rule}}</textarea>
    <fieldset class="rule-item-gui" v-else-if="rule">
      <form-item 
        v-for="([formItemName,formItemProps]) in Object.entries(ruleSchema)"
        :name="formItemName"
        :field-type="formItemProps.fieldType"
        :enums="enumDictionary[formItemName]"
        :answer="JSON.stringify(rule[formItemName])"
        :multiple="formItemProps.multiple"
        @change="formItemChange"
        v-model="rule[formItemName]"
      >
      </form-item>
      <ul>
        <li v-for="fe in rule['foreach']"><variable-item :variable-data="fe" ></variable-item></li>
      </ul>
      <ul>
        <li class="rule-field" :key="field" v-for="field in Object.keys(rule.fields)">
          <field-item v-model="rule.fields[field]"
            :field-object="rule.fields[field]"
            :name="field"
            :workspace="workspace"
            :available-rules="availableRules"
          >
          </field-item>
        </li>
      </ul>
    </fieldset>
  </section>
  `
}

