module.exports = function (functionName, params) {
    params = params || {};
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", this.kurmiinstance.api.config.restAPIpath, false);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    var formData = new FormData();
    formData.append("jsonData", JSON.stringify({
        "function": functionName,
        "parameters": params
    }));
    formData.append("action", "apiKurmi");
    xhttp.send(new URLSearchParams(formData).toString());
    return xhttp.responseText;
};
