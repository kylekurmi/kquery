const prismworker = require("prism_worker");

const prism_api_base = function($prism) {

    var $prism_api_base = this;
    var configParams = {
        auth: {},
        kurmiHost: null,
        apiPath: "../Kurmi/restAPI.do",
        substitutitionTenantOrTemplate: null
    }


    

    function getAuth() {
        return JSON.parse(JSON.stringify(configParams.auth));
    }

    function setAuth(login,password){
        configParams.auth.login = login;
        configParams.auth.password = password;
    }

    this.execute = function(functionName,params,tenantDbidOrName){
        params.auth = getAuth();
        if (tenantDbidOrName){
            params.auth.substitutionTenantOrTemplate  = getSubstitutionTenantOrTemplate(tenantDbidOrName);
        }
        else if (configParams.substitutionTenantOrTemplate) {
            params.auth.substitutionTenantOrTemplate  = substitutionTenantOrTemplate ;
        }
        
        
        return new prismworker([functionName,params],$prism.executeKurmiAPI);
    }


    const connectedEvents = [function(p){
        p.getTenants().then(function(res){
            p.tenants = res;
            $prism.debug("loaded tenants",JSON.stringify(p.tenants));
        });
        
    }];
    this.addConnectedEvent = function(callback){
        connectedEvents.push(callback);
    }

    this.isConnected = false;
    this.login = setAuth;
    this.connect = function() {
        
        return this.execute("getKurmiVersion",{}).then(function(res){
            connectedEvents.forEach(function(cb){
                cb($prism_api_base);
            });
            $prism.debug("connect",true);
            $prism_api_base.connected = true;
        }).catch(function(e){
            $prism.debug("connect",false,e);
            $prism_api_base.connected = false;
        });
    }

    var tenantList = null; 
    this.tenants = tenantList;
    this.getTenants = function() {
        return $prism.component.searchComponentList({type:"TENANT"});
    }

    function getSubstitutionTenantOrTemplate(tenantDbidOrName){
        var result = null;
        switch(typeof tenantDbidOrName){
            
            case typeof "tenantname": {
                try {
                    if (/SYSTEM/i.test(tenantDbidOrName)){
                        result = null;
                    }
                    //var numberDbid = tenantDbidOrName.replace(/\D/g,"");
                    //if (numberDbid && parseInt(numberDbid)){
                    //    result = getSubstituteTenantOrTemplate(parseInt(numberDbid));
                    //}
                    var tenant = $prism_api_base.tenants.filter(function(tenant){
                        return (new RegExp(tenantDbidOrName,"i")).test(tenant.identifier);
                    })[0];
                    result = {"@type": "TENANT", "@dbid": tenant["dbid"]};
                }
                catch(e){

                }

                break;
            }
            case typeof 1234: {

                break;
            }

        }
        $prism.debug("getSubstituteTenantOrTemplate",JSON.stringify([tenantDbidOrName,result]));
        return result;
    }

    this.substitute = function(tenantDbidOrName){
        configParams.substitutionTenantOrTemplate = getSubstitutionTenantOrTemplate(tenantDbidOrName);
        return true;
    }

    this.exitSubtitution = function() {
        delete configParams.auth.substitutionTenantOrTemplate;
        configParams.substitutionTenantOrTemplate = null;
        return true;
    }

}

module.exports = prism_api_base;
