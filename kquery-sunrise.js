const kurmi_sunrise_module = function(kurmiinstance){
    var $this = kurmiinstance;
    
    this.sunriseGetTenantDocuments = function (tenantName,wantedDocuments) {
        return this.kurmiinstance.api.execute("sunriseGetTenantDocuments",{tenant:tenantName,wantedDocument:[].concat(wantedDocuments)}).then(function(results){
            return results.document.reduce(function(res,doc){
                res[doc["@documentType"]] = JSON.parse(doc["#text"]);    
                return res;
            },{});
        });
    }       
    this.getTenantDocument = function(tenantName){
        return this.sunriseGetTenantDocuments(tenantName,"tenant")["tenant"];
    }
    this.getProvisioningDocument = function(tenantName){
        return this.sunriseGetTenantDocuments(tenantName,"provisioning")["provisioning"];
    }
    this.getSynchroDocument = function(tenantName){
        return this.sunriseGetTenantDocuments(tenantName,"synchro")["synchro"];
    }
    this.releaseLockForScenario = function(scenarioDBId){
        return this.kurmiinstance.api.execute("releaseLockForScenario",{scenarioDBId:scenarioDBId});
    }
    this.terminateSunriseScenario = function(scenarioDBId){
        return this.kurmiinstance.api.execute("releaseLockForScenario",{scenarioDBId:scenarioDBId});
    }
}


kurmibase.prototype.modules.push(["sunrise",kurmi_sunrise_module]);
