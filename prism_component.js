const prismworker = require("prism_worker");
const prism_component_base = function($prism) {
    var $prism_component_base = this;
    if (typeof searchExistingComponent != "undefined") {
        this.searchExistingComponent = searchExistingComponent && function() { 
            var args = arguments;
            return new prismworker(Object.keys(args).map(function(k){return args[k]; }),searchExistingComponent);
            
        }
    }
    else {
        this.searchExistingComponent = function(searchFilter, startingIndex, limitedToAdminScope) { 

        }
    }
    if (typeof searchComponentList != "undefined"){
        this.searchComponentList = searchComponentList && function() { 
            var args = arguments; 
            return new prismworker(Object.keys(args).map(function(k){return args[k]; }),searchComponentList); 
        }
    }
    else {
        this.searchComponentList = function (filter, startingIndex, returnLimit) {
            var params = { filter: filter, detail: true };
            if (filter["type"]) {
                filter["@type"] = filter["type"];
                delete filter["type"];
            }
            if (startingIndex) params.startingIndex = startingIndex;
            if (returnLimit) params.returnLimit = returnLimit;
            var componentWorker = {}
            return $prism.api.execute("searchQuery", params).then(function(componentResult){
                componentWorker.totalNumberOfResults = componentResult.totalNumberOfResults;
                if (componentResult && componentResult.componentDetail && componentResult.componentDetail[0]) {
                    return componentResult.componentDetail.map(function ($component) {
                        var _component = { "dbid": $component["@dbid"], "type": $component["@type"] };
                        if ($component.fields && $component.fields.field) {
                            $prism_component_base.fieldDeserialize($component.fields.field, _component);
                        }
                        if ($component.fields && $component.fields.fieldMult) {
                            //$this.api.component.fieldDeserialize($component.fields.field, _component);
                        }
                        return _component;
                    });
                }
                else if (componentResult && !componentResult.componentDetail) {
                    $prism.warn("[kQuery] searchQuery returned no components.", "componentResult.totalNumberOfResults: ", JSON.stringify(componentResult.totalNumberOfResults));
                    return null;
                }
                else {
                    return undefined;
                }
            });
        }

    }

    $prism_component_base.setConnectorVirtual = function (tenant, equipmentId, virtual) {
        return $prism.api.execute("searchConfig", { query: "/*/*[@equipmentId=" + equipmentId + "]" }, tenant).then(function(searchConfigQuery){
            if (!searchConfigQuery.path) {
                throw "[kQuery] No equipment found with equipmentId \"" + equipmentId + "\"" + (tenant ? " on tenant \"" + tenant + "\"" : "");
            }
            var query = virtual === true ? "*[not(boolean(@virtual))]" : "*[boolean(@virtual)]";
            return $prism.api.execute("searchConfig", { path: searchConfigQuery.path[0], query: query }, tenant).then(function(searchConfig){
                var setConfig = { configVersion: searchConfig.configVersion };

                setConfig.update = [searchConfigQuery.path].concat(searchConfig.path).map(function (path) {
                    return { "@path": path, key: { "@id": "virtual", "#text": virtual === true ? "true" : "false" } };
                });
                return $prism.api.execute("setConfig", setConfig, tenant);
            });
        });
    }
    $prism_component_base.processConfigNode = function processNode(_node, mergeWith) {
        if (_node.node) {

            if (/^\d+$/.test(_node.node[0]["@id"])) {
                mergeWith = _node.node.map(function (__node) {
                    return processNode(__node, {});
                });

            }
            else {
                _node.node.forEach(function (__node) {
                    mergeWith[__node["@id"]] = processNode(__node, {});
                });
            }

        }
        if (_node.key) {
            return _node.key.reduce(function (p, c) {
                p[c["@id"]] = c["#text"];
                return p;
            }, mergeWith);
        }
        else {
            return mergeWith;
        }
    }
    $prism_component_base.getEquipmentConfiguration = function (equipmentId, tenant) {
        var params = { query: "/*/*[@equipmentId=" + equipmentId + "]" };
        return $prism.api.execute("searchConfig", params, tenant).then(function(searchConfigQuery){
            if (!searchConfigQuery.path) {
                $prism.error("[prism]",JSON.stringify([params,searchConfigQuery]));
                throw "[kQuery] No equipment found with equipmentId \"" + equipmentId + "\"" + (tenant ? " on tenant \"" + tenant + "\"" : "");
            }
            var equipmentConfigurationRequest = searchConfigQuery.path && $kurmi.api.execute("getConfigTree", { path: searchConfigQuery.path[0], query: "[@equipmentId=" + equipmentId + "]" }, tenant).toJson();
            var equipmentConfiguration = equipmentConfigurationRequest && equipmentConfigurationRequest.configTree && $kurmi_component_module.processConfigNode(equipmentConfigurationRequest.configTree, {});
            return equipmentConfiguration;
        });        
    }

    $prism_component_base.fieldSerialize = function (fieldObject) {
        return Object.keys(fieldObject).map(function (key) {
            return {
                "@name": key,
                "#text": fieldObject[key]
            };
        });
    }

    $prism_component_base.fieldDeserialize = function (fieldsetList, baseObject, bFavorTextValueOverTechnicalValue) {
        return fieldsetList.reduce(function (p, fieldset, i, a) {
            if (bFavorTextValueOverTechnicalValue){
                p[fieldset["@name"]] = fieldset["#text"] || fieldset["@technicalValue"];
            }
            else {
                p[fieldset["@name"]] = fieldset["@technicalValue"] || fieldset["#text"];
            }
            
            return p;
        }, baseObject || {});
    }
}

module.exports = prism_component_base;