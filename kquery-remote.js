kurmiapimodule.prototype.setConfig = function (config) {
    this.config = config;
}
kurmiapimodule.prototype.setAuth = function (login, password) {
    this.config.authInfo.login = login;
    this.config.authInfo.password = password;
}
kurmiapimodule.prototype.getAuth = function () {
    return this.config.authInfo;
}
kurmiapimodule.prototype.login = function (login, password) {
    if (this.interactive) {
        login = login || prompt("Kurmi Login", "admin");
        password = password || prompt("Kurmi Password", "admin");
        this.setAuth(login, password);

    } else {
        console.warn("[kQuery]", "execute ./api/setAuth()");
    }
    return this.testConnection();
}
kurmiapimodule.prototype.testConnection = async function () {
    var api = this;
    return this.executeKurmiAPIAsync("getKurmiVersion", {
        auth: api.getAuth()
    }).then(function (res) {
        api.kurmiVersion = res.kurmiVersion;
        api.connected = true;
        console.log("[kQuery]", res);
        return res;
    }).catch(function (e) {
        api.connected = false;
        console.error("[kQuery]", e);
        throw e;
    });
}

kurmiapimodule.prototype.connected = false;
kurmiapimodule.prototype.interactive = true;
kurmiapimodule.prototype.config = {
    restAPIpath: "../../Kurmi/restAPI.do",
    authInfo: {
        login: null,
        password: null
    }
};

kurmiapimodule.prototype.execute = function (functionName, params, optionalTenantDbidOrName) {
    if (this.connected !== true) {
        throw "[kQuery] Not connected!";
    }
    var kw = new kurmiworker(null);
    try {

        params = params || {};
        params.auth = this.getAuth() || {};

        if (this.substitutionTenantOrTemplate) {
            params.auth.substitutionTenantOrTemplate = this.substitutionTenantOrTemplate;
        }
        if (optionalTenantDbidOrName) {
            params.auth.substitutionTenantOrTemplate = {
                "@type": "TENANT",
                "@dbid": this.getTenantDbid(optionalTenantDbidOrName)
            };
        }

        kw.response = JSON.parse(this.executeKurmiAPI(functionName, params));

        var resp = kw.response;
        if (resp.status && resp.errorDetail) {
            kw.errorData = resp;
            kw.errorMessage = resp.errorDetail;
        }
        else if (resp.status && resp.status == "SUCCESS") {
            kw.successData = resp;

        }
        if (resp.status && resp.status != "SUCCESS") {
            kw.errorData = resp;
            kw.errorMessage = resp.errorMessage;
        }
        if (resp && resp.messages) {
            kw.errorData = resp;
            kw.errorMessage = resp.messages.length > 1 ? JSON.stringify(resp.messages) : resp.messages[0].message || resp.messages[0];
        }
    } catch (e) {
        kw.errorMessage = e;
    }

    return kw;
}

if (window) {
    //we assume this is browser mode
    kurmiapimodule.prototype.executeKurmiAPI = kurmiapimodule.prototype.executeKurmiAPIbrowser; 
} 
else if (executeKurmiAPI){
    //we assume we are running in the kurmi scripting layer on the kurmi server
    kurmiapimodule.prototype.executeKurmiAPI = executeKurmiAPI;
}
else if (getCurrentTenant && equipmentId) {
    //we assume we are running in the connectorsdk script
    kurmiapimodule.prototype.executeKurmiAPI = kurmiapimodule.prototype.executeKurmiAPIsdk;
}



kurmiapimodule.prototype.executeKurmiAPIsdk = function (functionName, params) {
    var apiRequest = {
        'function': action,
        'parameters': params
    };
    var curlRequest = "curl -X POST -k -Ss -w 'HTTP_STATUS_CODE:%{http_code}\\n' -H 'Content-Type: application/x-www-form-urlencoded' 'https://localhost/"+this.config.restAPIpath.replace(/\.\.\//g,"")+"'"
        + " --data 'action=apiKurmi'"
        + " --data-urlencode 'jsonData=" + JSON.stringify(apiRequest).replace(/'/g, "'\\''") + "'";
    try {
        var curlResultStr = sendCommand(curlRequest);
    } catch (e) {
        console.error("[/connector.xml] [callKurmiAPI] The sendCommand faced an error: " + e); // Displayed in logs, not the GUI
        throw "An error has been faced while trying to call an internal API."; // Displayed in both logs and GUI
    }

    var curlResultParsed = /^(.*)HTTP_STATUS_CODE:(\d{3})$/.exec(curlResultStr)
    if (curlResultParsed.length > 1) {
        var httpStatus = curlResultParsed[2];

        console.debug('[/connector.xml] [callKurmiAPI] HTTP status: )))' + httpStatus + '(((');

        if (httpStatus == '200') {
            console.debug('[/connector.xml] [callKurmiAPI] JSON Result: )))' + curlResultParsed[1] + '(((');
            return JSON.parse(curlResultParsed[1]);
        } else {
            throw 'Problem: ' + curlResultStr;
        }
    } else {
        throw 'Problem: ' + curlResultStr;
    }

}

kurmiapimodule.prototype.executeKurmiAPIbrowser = function (functionName, params) {
    params = params || {};
    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", this.kurmiinstance.api.config.restAPIpath, false);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    var formData = new FormData();
    formData.append("jsonData", JSON.stringify({
        "function": functionName,
        "parameters": params
    }));
    formData.append("action", "apiKurmi");
    xhttp.send(new URLSearchParams(formData).toString());
    return xhttp.responseText;
}

kurmiapimodule.prototype.executeKurmiAPIAsync = function (functionName, params) {
    params = params || {};
    var $this = this;
    var $promise = new Promise(function (resolve, reject) {
        try {
            var formData = new FormData();
            formData.append("jsonData", JSON.stringify({
                "function": functionName,
                "parameters": params
            }));
            formData.append("action", "apiKurmi");
            fetch($this.config.restAPIpath, {
                method: 'POST',
                credentials: "include",
                mode: 'no-cors',
                body: formData
            }).then(function (resp) {
                return resp.json();
            }).then(function (jResp) {
                if (jResp.status && jResp.errorDetail) {
                    reject(jResp);
                }
                else if (jResp.status && jResp.status == "SUCCESS") {
                    resolve(jResp);
                } else if (jResp.status && jResp.status != "SUCCESS") {
                    reject(jResp);
                } else {
                    throw "[kQuery][unexpected json result] " + JSON.stringify(jResp);
                }

            }).catch(function (e) {
                reject(e);
            });

        } catch (e) {
            reject({
                status: "FAIL",
                message: e
            });
        }
    });

    return $promise;
}