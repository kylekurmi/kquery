import jq from './jQuery.js'
$.noConflict();
var jQuery = jq.noConflict();
var kQuery = {};

kQuery._authInfo = localStorage["executeKurmiAPI_auth"] && JSON.parse(localStorage["executeKurmiAPI_auth"]) || {
    kurmiLogin: null,
    kurmiPassword: null
};
kQuery.authInfo = function() {
    if (!kQuery._authInfo.kurmiLogin) {
        kQuery._authInfo.kurmiLogin = prompt("Kurmi Login", "admin");
    }
    if (!kQuery._authInfo.kurmiPassword) {
        kQuery._authInfo.kurmiPassword = prompt("Kurmi Password", "admin");
    }
    localStorage["executeKurmiAPI_auth"] = JSON.stringify(kQuery._authInfo);
    return kQuery._authInfo;
}

kQuery._tenantDbLookup = localStorage["executeKurmiAPI_tenantDbLookup"] && JSON.parse(localStorage["executeKurmiAPI_tenantDbLookup"]) || {};
kQuery.getTenantDbId = async function(tenantName) {
    if (kQuery._tenantDbLookup[tenantName] === undefined) {
        let dbidRes = await kQuery.executeKurmiAPI("searchQuery", {
            filter: {
                "@type": "TENANT",
                field: {
                    "@name": "identifier",
                    "@criteria": "Equals",
                    "#text": tenantName
                }
            }
        });
        dbidRes = JSON.parse(dbidRes);
        let dbid = dbidRes.status == "SUCCESS" && dbidRes.component && dbidRes.component[0] && dbidRes.component[0]["@dbid"];
        if (dbidRes.status != "SUCCESS") {
            throw JSON.stringify(dbidRes);
        }
        kQuery._tenantDbLookup[tenantName] = dbid;
        localStorage["executeKurmiAPI_tenantDbLookup"] = JSON.stringify(kQuery._tenantDbLookup);
    } else {
    }
    return kQuery._tenantDbLookup[tenantName];
}

kQuery.currentTenantName = null;
kQuery.currentTenantDbId = null;

kQuery.substitute = function(tenantName) {
    kQuery.getTenantDbId(tenantName).then(function(dbid) {
        if (!dbid) {
            throw `Tenant "${tenantName}" could not be found!`;
        }
        kQuery.currentTenantDbId = dbid;
        kQuery.currentTenantName = tenantName;
        console.log("Substituted to " + tenantName);
    }).catch(function(e) {
        console.error(e);
    });

}
kQuery.exitSubstitution = function() {
    console.log("Exited substitution from " + kQuery.currentTenantName);
    kQuery.currentTenantName = undefined;
    kQuery.currentTenantDbId = undefined;
}

kQuery.apiMethods = {};
kQuery.apiMethodNames = [];
kQuery.wsdlStorage = {};

kQuery.loadApiMethods = function() {
    if (!jQuery && !jQuery.get)
        throw "no Jquery!";
    return jQuery.get("../../Kurmi/APIService?wsdl").then(function(data) {
        kQuery.wsdlStorage["main"] = jQuery(data);
        jQuery(data).find('message').each(function(i, item) {
            kQuery.apiMethodNames.push(jQuery(item).attr('name'));
            var name = jQuery(item).attr('name');
            kQuery.apiMethods[name] = {
                element: (jQuery(item).find('part').attr('element') || ":").split(':')[1],
                getParams: function() {
                    return kQuery.getParamsForAPIMethod(name);
                }
            };
            kQuery.apiMethods[name].execute = async function(params, tenantName) {
                return JSON.parse(await kQuery.executeKurmiAPI(name, params || {}, tenantName));
            }
        });
        console.log("Loaded API Methods. kQuery is ready!");
        kQuery.ready = true;
    }).fail(function() {
        console.error("Failed to load SOAP API METHODS!");
        kQuery.ready = false;
    });

}

kQuery.wsdlResolver = function(wsdl, queryselector, objectIn){
	
	jQuery(wsdl).find(queryselector).each(function(i,item) {
	    jQuery(item).find('xs\\:element').each(function(ii, iitem) {
			var name = jQuery(iitem).attr('name');
			var type = jQuery(iitem).attr('type');
			var objectToPass = {};
			if (objectIn[name] && typeof objectIn[name] === typeof {}) {
				objectIn[name] = [objectIn[name]];
			}
			if (objectIn[name] && typeof objectIn[name] === typeof []) {
				objectToPass = {};
				objectIn[name][objectIn[name].length] = objectToPass;
				
			} else {
				objectIn[name] = {};
				objectToPass = objectIn[name];
			}
			kQuery.wsdlResolver(wsdl, `xs\\:complexType[name="${type}"]`, objectToPass);
		});
	});
	
	
}


kQuery.getParamsForAPIMethod = function(methodName) {
    methodName = (kQuery.apiMethods[methodName] || {}).element || methodName;
    if (!kQuery.wsdlStorage["main"]) {
        kQuery.loadApiMethods();
    }
    var schemaLocation = jQuery(kQuery.wsdlStorage["main"]).find('types [namespace=\"http://www.nates.fr/API/1.0\"]').attr('schemaLocation');
    var params = [];
    var data = jQuery.ajax({
        type: "GET",
        url: schemaLocation,
        async: false
    }).responseText;
    console.log(jQuery(data).find(`xs\\:element[name="${methodName}"]`).html());
    jQuery(data).find(`xs\\:element[name="${methodName}"]`).each(function(i, item) {
        jQuery(item).find('xs\\:element').each(function(ii, iitem) {
            var name = jQuery(iitem).attr('name');
			var xmltype = (jQuery(iitem).attr('type') || "").replace("tns:","");
			if (xmltype) {
				var typp = jQuery(data).find(`xs\\:complexType[name="${xmltype}"]`);
				console.log("type", jQuery(typp).html());	
				params.push(jQuery(iitem).attr('name'));
			}
			else {
				params.push(jQuery(iitem).attr('name'));
			}
        });

    });
    console.log(params);
    return params;
    //console.log("Loaded API Methods. kQuery is ready!");

}

kQuery.loadApiMethods();

kQuery.executeKurmiAPI = async function(action, params, tenantName) {

    var req = {
        "function": action,
        "parameters": params || {}
    };
    req.parameters.auth = {
        "login": kQuery.authInfo().kurmiLogin,
        "password": kQuery.authInfo().kurmiPassword
    };
    
    if (kQuery.currentTenantName && kQuery.currentTenantDbId && !tenantName) {
        tenantName = kQuery.currentTenantName;
    }

    if (tenantName) {
        let tenantDbid = await kQuery.getTenantDbId(tenantName);
        if (!tenantDbid) {
            throw "Didnt Get the tenantDbId"
        }
        req.parameters.auth.substitutionTenantOrTemplate = {
            "@type": "TENANT",
            "@dbid": tenantDbid
        };
    }

    //return window.frames[0].$.ajax({
    return jQuery.ajax({
        url: "../../Kurmi/restAPI.do",
        type: "post",
        data: {
            action: "apiKurmi",
            jsonData: JSON.stringify(req)
        }
    }).done(function(resp) {
        var respObj = JSON.parse(resp);
        if (respObj.status == "SUCCESS") {//console.info("Successful request and response!");
        //console.log("Response: " + resp);
        } else {
            console.warn("Successful request BUT got and error in return!");
            console.error("Response: " + resp);
        }
        return respObj;
    }).fail(function(resp) {
        var toLog = resp.responseText ? resp.responseText : resp;
        console.error(toLog);
    });

}

kQuery.searchComponentList = async function(searchFilter, startingIndex, limitedToAdminScope) {

    var soapFilter = {
        filter: Object.keys(searchFilter).filter(f=>!/type/.test(f)).reduce((p,c,i,a)=>{
            switch (typeof searchFilter[c]) {
            case typeof {}:
                {
                    p.field.push(Object.keys(searchFilter[c]).reduce((pp,cc,ii,aa)=>{
                        switch (cc) {
                        case "value":
                            {
                                pp["#text"] = searchFilter[c]["value"];
                                break;
                            }
                        default:
                            {
                                pp["@" + cc] = searchFilter[c][cc];
                                break;
                            }
                        }
                        return pp;
                    }
                    , {
                        "@name": c
                    }));

                    break;
                }
            case typeof "":
                {
                    p.field.push({
                        "@name": c,
                        "@criteria": "Equals",
                        "#text": searchFilter[c]
                    });
                    break;
                }
            }

            return p;
        }
        , {
            "@type": searchFilter.type,
            field: []
        })

    };
    var res = await executeKurmiAPI("searchQuery", soapFilter, currentTenantName).then(function(res) {

        var resObj = JSON.parse(res);
        var listOfComponents = [];
        if (resObj.status == "SUCCESS" && resObj.component) {

            resObj.component.forEach(async function(component) {
                return await executeKurmiAPI("detailQuery", {
                    component: component
                }, currentTenantName).then(function(_res) {
                    var _resObj = JSON.parse(_res);
                    if (_resObj.status == "SUCCESS" && _resObj.component) {
                        var componentFormatted = _resObj.component.fields.field.reduce(function(p, c, i, a) {
                            p[c["@name"]] = c["@technicalValue"] || c["#text"];
                            p.textValues[c["@name"]] = c["#text"];
                            return p;
                        }, {
                            textValues: {}
                        });
                        componentFormatted.dbid = _resObj.component["@dbid"];
                        componentFormatted.type = _resObj.component["@type"];
                        listOfComponents.push(componentFormatted);
                    }
                });
            });
        }
        return listOfComponents;
    });
    return res;
}

kQuery.auditType = {
    None: "None",
    New: "New",
    Optimized: "Optimized",
    Core: "Core",
    Full: "Full",
    Inference: "Inference"
};
kQuery.auditSync = {
    None: "None",
    DB: "DB",
    System: "System"
};

kQuery.auditResource = async function(filter, audit_None_New_Optimized_Core_Full_Inference, sync_None_DB_System, isAsync) {
    var soapFilter = filter;
    var params = {
        "filter": soapFilter,
        "exploration": audit_None_New_Optimized_Core_Full_Inference,
        "synchronization": sync_None_DB_System,
        "async": isAsync
    };
    let auditResult = await kQuery.executeKurmiAPI("auditResource", params, currentTenantName);
    console.log(JSON.stringify(auditResult));
}

kQuery.listWorkspaces = async function(bFlat) {
    let res = JSON.parse(await kQuery.executeKurmiAPI("listWorkspaces"));
    if (res.status && res.status == "SUCCESS" && res.workspaces) {
        var workspaces = JSON.parse(res.workspaces);
        if (bFlat == true) {
            var fWorkspaces = [];
            var flatten = function(node) {
                fWorkspaces.push(node.name);
                if (node.childs) {
                    node.childs.forEach(function(child) {
                        fWorkspaces.push(child.name);
                        flatten(child);
                    });
                }
            }
            workspaces.forEach((ws)=>flatten(ws));
            workspaces = fWorkspaces;
        }
        return workspaces;
    }
}

kQuery.listWorkspaceFiles = async function(workspaceName) {
    let res = JSON.parse(await kQuery.executeKurmiAPI("listWorkspaceFiles", {
        workspace: workspaceName
    }));
    if (res.status && res.status == "SUCCESS" && res.files) {
        return JSON.parse(res.files).map(f=>f.path);
    }
}

kQuery.getWorkspaceFile = async function(workspaceName, filePath) {
    let res = JSON.parse(await kQuery.executeKurmiAPI("getWorkspaceFile", {
        workspace: workspaceName,
        path: filePath
    }));
    if (res.status && res.status == "SUCCESS") {
        return res;

    }
}

kQuery.addWorkspaceFile = async function(workspaceName, fileResult) {
    var addWorkspaceFileParams = JSON.parse(JSON.stringify(fileResult, ["path", "description", "revisionAuthor", "revisionComment", "content"]));
    addWorkspaceFileParams.workspace = workspaceName;
    let res = JSON.parse(await kQuery.executeKurmiAPI("addWorkspaceFile", addWorkspaceFileParams));
}

kQuery.addWorkspaceFiles = async function(workspaceName, fileResults) {
    var files = fileResults.map(function(rs) {
        return JSON.parse(JSON.stringify(rs, ["path", "description", "revisionAuthor", "revisionComment", "content"]));
    });
    var addWorkspaceFileParams = {
        file: files
    };
    addWorkspaceFileParams.workspace = workspaceName;
    let res = JSON.parse(await executeKurmiAPI("addWorkspaceFiles", addWorkspaceFileParams));
}

kQuery.getCCM9pathByEquipmentId = async function(equipmentId, tenantName) {
    let res = JSON.parse(await executeKurmiAPI("searchConfig", {
        path: "/CCM9_0 call server",
        query: "*[@equipmentId=" + equipmentId + "]"
    }, tenantName));
    return res && res.path && res.path[0];
}

kQuery.fieldSerialize = function(fieldObject) {
    return Object.keys(fieldObject).map(function(key) {
        return {
            "@name": key,
            "#text": fieldObject[key]
        };
    });
}

kQuery.fieldDeserialize = function(fieldsetList) {
    return fieldsetList.reduce(function(p, fieldset, i, a) {
        p[fieldset["@name"]] = fieldset["#text"] || fieldset["@technicalValue"];
        return p;
    }, {});
}
;

kQuery.getComponentKind = async function(noCache){
    var lsPath = "executeKurmiAPI_componentkind";
    var storedComponents = localStorage[lsPath];
    var componentTenantMap =  storedComponents && JSON.parse(storedComponents) || {};
    var components = componentTenantMap[kQuery.currentTenantName || "kurmi"] || [];

    if (!components.length || noCache){
        let sqlRequest = JSON.parse(await kQuery.executeKurmiAPI("executeReadSQL", {sqlScript: "Select name from componentkind;"}));
        var components = sqlRequest.status && sqlRequest.status == "SUCCESS" && sqlRequest.sqlLineResult.map(function(row) { return row.item[0]});
        componentTenantMap[kQuery.currentTenantName || "kurmi"] = components;

        localStorage[lsPath] = JSON.stringify(componentTenantMap);
    }
    console.log(kQuery.currentTenantName || "kurmi", components);
    return components;
}

kQuery.getServiceTypes = async function(noCache){
    var lsPath = "executeKurmiAPI_servicetypes";
    var storedServiceTypes = localStorage[lsPath];
    var serviceTypesTenantMap =  storedServiceTypes && JSON.parse(storedServiceTypes) || {};
    var serviceTypes = serviceTypesTenantMap[kQuery.currentTenantName || "kurmi"] || [];

    if (!serviceTypes.length || noCache){
        let sqlRequest = JSON.parse(await kQuery.executeKurmiAPI("executeReadSQL", {sqlScript: "Select serviceid from service_type;"}));
        var serviceTypes = sqlRequest.status && sqlRequest.status == "SUCCESS" && sqlRequest.sqlLineResult.map(function(row) { return row.item[0]});
        serviceTypesTenantMap[kQuery.currentTenantName || "kurmi"] = serviceTypes;

        localStorage[lsPath] = JSON.stringify(serviceTypesTenantMap);
    }
    console.log(kQuery.currentTenantName || "kurmi", serviceTypes);
    return serviceTypes;
}

kQuery.quickFeatureMapping = async function(workspaceName) {
    var qfmapping = {};
    var serviceMapping = {};
    var newServiceCreationFromQF = {};
    var qfs = await kQuery.listWorkspaceFiles(workspaceName);
    qfs = qfs.filter(f=>/quickfeature.xml/i.test(f));

    var doneQfs = new Promise((resolve,reject)=>{
        qfs.forEach(async function(qfpath,i,arr) {
            kQuery.getWorkspaceFile(workspaceName,qfpath).then(function(x) {
                qfmapping[qfpath] = x && x.content && x.content.match(/<service-addon(.+?)\>/g);
                var isNewServiceCreation = x && x.content && x.content.match(/newService\(['"](.+?)['")]/);
                if (isNewServiceCreation){
                    newServiceCreationFromQF[isNewServiceCreation[1] || ""] = newServiceCreationFromQF[isNewServiceCreation[1] || ""] || [];
                    newServiceCreationFromQF[isNewServiceCreation[1] || ""].push(qfpath);
                }
                if (arr.length == i+1){
                    resolve(qfmapping);
                }     
            });
        });

    });

    return await doneQfs.then(function(){
        
        
        var result =  Object.entries(qfmapping).reduce(function(p,[qfpath,triggers]){
            var services = triggers && triggers.reduce(function(pp,cc) {
            var serviceName = cc.match(/(?<=identifier=")(.+?)"/i);
            serviceName = serviceName && serviceName[1] || cc; 
            pp[serviceName] = pp[serviceName] || {};
            var trigger = cc.match(/(?<=trigger=")(.+?)"/i); 
            trigger = trigger && trigger[1] || "apply-to";
            pp[serviceName][trigger] = pp[serviceName][trigger] || [];
            pp[serviceName][trigger].push(qfpath);
             if (newServiceCreationFromQF[serviceName]) {
                pp[serviceName]["created-in"] = newServiceCreationFromQF[serviceName];
             }
            return pp; 
            },p);
            
                
            return p;
            },{});
        Object.entries(newServiceCreationFromQF).reduce(function(p,[key,val]){
       p[key] = p[key] || {};
       p[key]["created-in"] = p[key]["created-in"] || val;
       return p;
   },result);

   return result;
    });
   
}

kQuery.tenants = {};
kQuery.getTenants = async function(reload){
    var lsPath = "executeKurmiAPI_tenantDbLookup";
    kQuery._tenantDbLookup = localStorage[lsPath] && JSON.parse(localStorage[lsPath]) || {};
    
    if (reload){
        kQuery.exitSubstitution();
        let sqlRequest = JSON.parse(await kQuery.executeKurmiAPI("executeReadSQL", {sqlScript: "select auxiliary_id, identifier, description from tenant;"}));
        if (sqlRequest.status && sqlRequest.status == "SUCCESS") {
            kQuery._tenantDbLookup = sqlRequest.sqlLineResult.reduce(function(p,row,i,a) {
                if (!kQuery.tenants[row.item[0]]) {
                    kQuery.tenants[row.item[0]] = {identifier: row.item[1], auxiliaryid: row.item[0], description: row.item[2]};
                }
                
                p[row.item[0]] = row.item[1];
            return p}, {});
            console.log(sqlRequest.sqlLineResult.map(function(row){
                return row.item.join('\t\t');
            }).join('\r\n'));
        }
        else {
                        
        }
        localStorage[lsPath] = JSON.stringify(kQuery._tenantDbLookup);
    }
   
    return kQuery._tenantDbLookup;
}





var $kurmi = function(param) {
    if (/^#\w+/.test(param)) {
        try {
            kQuery.substitute(param.match(/#(\w+)/)[1]);
        } catch (e) {
            throw e;
        }

    }
    if (/coreuser/.test(param)) {
        var mtch = param.match(/coreuser\[(\w+)=(\w+)\]/);
        var [fieldName,fieldValue] = [mtch[1], mtch[2]];
        return kQuery.apiMethods.searchQuery.execute({
            filter: {
                "@type": "coreuser",
                field: {
                    "@name": fieldName,
                    "@criteria": "Equals",
                    "#text": fieldValue
                }
            }
        });

    }

    return kQuery;

}

export default kQuery;