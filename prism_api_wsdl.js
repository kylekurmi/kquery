const prism_api_wsdl = async function($prism){
    $prism.api.apiMethods = {};
    $prism.api.apiMethodNames = [];
    $prism.api.wsdlStorage = {};


    function parseWsdl(data) {
        let doc = document.implementation.createHTMLDocument("Kurmi WSDL Document");
        doc.write(data);
        $prism.api.wsdlStorage["main"] = data;
        doc.querySelectorAll('message').forEach(function (item, i) {
            $prism.api.apiMethodNames.push(item.getAttribute('name'));
            var name = item.getAttribute('name');
            /*
            $kurmiapimodule.apiMethods[name] = function (params, tenantName) {
                return $kurmiapimodule.execute(name, params || {}, tenantName).toJSON();
            }

            $kurmiapimodule.apiMethods[name].element = (item.querySelector('part').getAttribute('element') || ":").split(':')[1];
            $kurmiapimodule.apiMethods[name].getParams = function () {
                    return $kurmiapimodule.getParamsForAPIMethod(name);
                }
            $kurmiapimodule.apiMethods[name].execute = function (params, tenantName) {
                return $kurmiapimodule.execute(name, params || {}, tenantName).toJSON();
            }
            */
            $kurmiapimodule[name] = function (params, tenantName) {
                return $prism.api.execute(name, params || {}, tenantName);
            }

            $prism.api[name].element = (item.querySelector('part').getAttribute('element') || ":").split(':')[1];
            $prism.api[name].getParams = function () {
                return getParamsForAPIMethod(name, $prism.api[name].element);
            }
            $prism.api[name].getPrototype = function () {
                return getFunctionForParams(name, $prism.api[name].element);
            }

                getFunctionForParams
                $prism.api[name].execute = function (params, tenantName) {
                return $prism.api.execute(name, params || {}, tenantName);
            }
        });
        $prism.log("Loaded API Methods, ready!");
    
    };

    function wsdlResolver(wsdl, queryselector, objectIn) {

        (wsdl).querySelectorAll(queryselector).forEach(function (i, item) {
            jQuery(item).querySelectorAll('xs\\:element').forEach(function (iitem, ii) {
                var name = (iitem).getAttribute('name');
                var type = (iitem).getAttribute('type');
                var objectToPass = {};
                if (objectIn[name] && typeof objectIn[name] === typeof {}) {
                    objectIn[name] = [objectIn[name]];
                }
                if (objectIn[name] && typeof objectIn[name] === typeof []) {
                    objectToPass = {};
                    objectIn[name][objectIn[name].length] = objectToPass;

                } else {
                    objectIn[name] = {};
                    objectToPass = objectIn[name];
                }
                $prism.api.wsdlResolver(wsdl, `xs\\:complexType[name="${type}"]`, objectToPass);
            });
        });
    }

    async function getParamsForAPIMethod(methodName,elementName) {
        methodName = ($prism.api.apiMethods[methodName] || {}).element || methodName;
        if (!$prism.api.wsdlStorage["main"]) {
            $prism.api.loadApiMethods();
        }
        let doc = document.implementation.createHTMLDocument("New Document");
        doc.write($prism.api.wsdlStorage["main"])
        var schemaLocation = doc.querySelector('types [namespace=\"http://www.nates.fr/API/1.0\"]').getAttribute('schemaLocation');
        var params = [];
        var data = await (await (fetch(schemaLocation))).text();
        let doc2 = document.implementation.createHTMLDocument("New Document");
        doc2.write(data);
        $prism.log(doc2.querySelector(`xs\\:element[name="${elementName}"]`).outerHTML);
        doc2.querySelectorAll(`xs\\:element[name="${elementName}"]`).forEach(function (item, i) {
            item.querySelectorAll('xs\\:element').forEach(function (iitem, ii) {
                var name = (iitem).getAttribute('name');
                var xmltype = ((iitem).getAttribute('type') || "").replace("tns:", "");
                if (xmltype) {
                    //var typp = (doc2).querySelector(`xs\\:complexType[name="${xmltype}"]`);
                    //console.log("type", (typp).outerHTML);
                    params.push((iitem).getAttribute('name'));
                }
                else {
                    params.push((iitem).getAttribute('name'));
                }
            });
            item.querySelectorAll('xs\\:extension').forEach(function (iitem, ii) {
                var base = (iitem).getAttribute('base').split(':')[1];
                params.push(base);
                $prism.log(doc2.querySelector(`xs\\:extension[base="${(iitem).getAttribute('base')}"]`).outerHTML);
            });
        });
        $prism.log(params);
        return params;
    }

    $prism.api.wsdlTypes = {};
    var wsdlTypes = $prism.api.wsdlTypes;

    function processParam(item){
        var name = item.getAttribute('name');
        var minOccurs = parseInt(item.getAttribute('minOccurs')|| "-1");
        var prefix = "o";
        switch(item.getAttribute('type')){
            case "xs:string": {
                prefix = "s";
                break;
            }
            case "xs:boolean":{
                prefix = "b";
                break;
            }
            case "xs:int": {
                prefix = "i";
                break;
            }
                        
        }
        var paramName = prefix + name.replace(/^(.)/,function(r,fm) { return fm.toUpperCase();})
        paramName = minOccurs == 0 ? "optional_" + paramName : paramName;
        var functionLogic = minOccurs == 0 ? `\tif (${paramName}) this["${paramName}"] = ${paramName};` : `\tthis["${paramName}"] = ${paramName};`;
        paramName = paramName.replace(/^(.)/,function(r,fm) { return fm.toLowerCase();});
        return [paramName,functionLogic];
    }

    async function getFunctionForParams(methodName,elementName) {
        methodName = ($prism.api.apiMethods[methodName] || {}).element || methodName;
        if (!$prism.api.wsdlStorage["main"]) {
            $prism.api.loadApiMethods();
        }
        let doc = document.implementation.createHTMLDocument("New Document");
        doc.write($prism.api.wsdlStorage["main"])
        var schemaLocation = doc.querySelector('types [namespace=\"http://www.nates.fr/API/1.0\"]').getAttribute('schemaLocation');
        var params = [];
        var functionLines = [];
        var data = await (await (fetch(schemaLocation))).text();
        let doc2 = document.implementation.createHTMLDocument("New Document");
        doc2.write(data);
        $prism.log(doc2.querySelector(`xs\\:element[name="${elementName}"]`).outerHTML);


        doc2.querySelectorAll(`xs\\:element[name="${elementName}"]`).forEach(function (item, i) {
            
            item.querySelectorAll('xs\\:element').forEach(function (iitem, ii) {
                var [paramName,functionLogic] = processParam(iitem);
                params.push(paramName);
                functionLines.push(functionLines);
            });
            item.querySelectorAll('xs\\:extension').forEach(function (iitem, ii) {
                var base = (iitem).getAttribute('base').split(':')[1];
                
                doc2.querySelectorAll(`xs\\:complexType[name="${base}"] xs\\:element`).forEach(function(iiitem){
                    var [paramName,functionLogic] = processParam(iiitem);
                    params.push(paramName);
                    functionLines.push(functionLines);
                });
                                
            });
        });
        $prism.log(params);

        return new Function(params, functionLines.join('\r\n'));
        //return params;
    }

    var wsdl = localCache.get("wsdl", "main");
    if (wsdl) {
        return parseWsdl(wsdl);
    }


    return fetch($prism.api.configParams.apiPath.replace("restAPI.do","APIService?wsdl")).then(function (data) {
        return data.text();
    }).then(parseWsdl).catch(function (e) {
        $prism.error("Failed to load SOAP API METHODS!");
        $prism.api.ready = false;
    });


}

module.exports = prism_api_wsdl;

