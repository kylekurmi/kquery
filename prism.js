require=(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({"prism_api_base":[function(require,module,exports){
const prismworker = require("prism_worker");

const prism_api_base = function($prism,executeKurmiAPI) {

    var $prism_api_base = this;
    var configParams = {
        auth: {},
        kurmiHost: null,
        apiPath: "../Kurmi/restAPI.do",
        substitutitionTenantOrTemplate: null
    }

    if (typeof executeKurmiAPI == "undefined"){
        var executeKurmiAPI = function (functionName, params) {
            params = params || {};
            var $this = this;
            var $promise = new Promise(function (resolve, reject) {
                try {
                    var formData = new FormData();
                    formData.append("jsonData", JSON.stringify({
                        "function": functionName,
                        "parameters": params
                    }));
                    formData.append("action", "apiKurmi");
                    fetch(configParams.apiPath, {
                        method: 'POST',
                        credentials: "include",
                        mode: 'no-cors',
                        body: formData
                    }).then(function (resp) {
                        return resp.json();
                    }).then(function (jResp) {
                        if (jResp.status && jResp.errorDetail) {
                            reject(jResp);
                        }
                        else if (jResp.status && jResp.status == "SUCCESS") {
                            resolve(jResp);
                        } else if (jResp.status && jResp.status != "SUCCESS") {
                            reject(jResp);
                        } else {
                            throw "[kQuery][unexpected json result] " + JSON.stringify(jResp);
                        }
        
                    }).catch(function (e) {
                        reject(e);
                    });
        
                } catch (e) {
                    reject({
                        status: "FAIL",
                        message: e
                    });
                }
            });
        
            return $promise;
        }
    }
    

    function getAuth() {
        return JSON.parse(JSON.stringify(configParams.auth));
    }

    function setAuth(login,password){
        configParams.auth.login = login;
        configParams.auth.password = password;
    }




    this.execute = function(functionName,params,tenantDbidOrName){
        params.auth = getAuth();
        if (tenantDbidOrName){
            params.auth.substitutionTenantOrTemplate  = getSubstitutionTenantOrTemplate(tenantDbidOrName);
        }
        else if (configParams.substitutionTenantOrTemplate) {
            params.auth.substitutionTenantOrTemplate  = substitutionTenantOrTemplate ;
        }
        
        
        return new prismworker([functionName,params],executeKurmiAPI);
    }


    const connectedEvents = [function(p){
        p.getTenants().then(function(res){
            p.tenants = res;
            console.debug("loaded tenants",JSON.stringify(p.tenants));
        });
        
    }];
    this.addConnectedEvent = function(callback){
        connectedEvents.push(callback);
    }

    this.isConnected = false;
    this.login = setAuth;
    this.connect = function() {
        
        return this.execute("getKurmiVersion",{}).then(function(res){
            connectedEvents.forEach(function(cb){
                cb($prism_api_base);
            });
            console.debug("connect",true);
            $prism_api_base.connected = true;
        }).catch(function(e){
            console.debug("connect",false,e);
            $prism_api_base.connected = false;
        });
    }

    var tenantList = null; 
    this.tenants = tenantList;
    this.getTenants = function() {
        return $prism.component.searchComponentList({type:"TENANT"});
    }

    function getSubstitutionTenantOrTemplate(tenantDbidOrName){
        var result = null;
        switch(typeof tenantDbidOrName){
            
            case typeof "tenantname": {
                try {
                    if (/SYSTEM/i.test(tenantDbidOrName)){
                        result = null;
                    }
                    //var numberDbid = tenantDbidOrName.replace(/\D/g,"");
                    //if (numberDbid && parseInt(numberDbid)){
                    //    result = getSubstituteTenantOrTemplate(parseInt(numberDbid));
                    //}
                    var tenant = $prism_api_base.tenants.filter(function(tenant){
                        return (new RegExp(tenantDbidOrName,"i")).test(tenant.identifier);
                    })[0];
                    result = {"@type": "TENANT", "@dbid": tenant["dbid"]};
                }
                catch(e){

                }

                break;
            }
            case typeof 1234: {

                break;
            }

        }
        console.debug("getSubstituteTenantOrTemplate",JSON.stringify([tenantDbidOrName,result]));
        return result;
    }

    this.substitute = function(tenantDbidOrName){
        configParams.substitutionTenantOrTemplate = getSubstitutionTenantOrTemplate(tenantDbidOrName);
        return true;
    }

    this.exitSubtitution = function() {
        delete configParams.auth.substitutionTenantOrTemplate;
        configParams.substitutionTenantOrTemplate = null;
        return true;
    }

}

module.exports = prism_api_base;
},{"prism_worker":"prism_worker"}],"prism_component_base":[function(require,module,exports){
const prismworker = require("prism_worker");
const prism_component_base = function($prism) {
    var $prism_component_base = this;
    if (typeof searchExistingComponent != "undefined") {
        this.searchExistingComponent = searchExistingComponent && function() { 
            var args = arguments;
            return new prismworker(Object.keys(args).map(function(k){return args[k]; }),searchExistingComponent);
            
        }
    }
    else {
        this.searchExistingComponent = function(searchFilter, startingIndex, limitedToAdminScope) { 

        }
    }
    if (typeof searchComponentList != "undefined"){
        this.searchComponentList = searchComponentList && function() { 
            var args = arguments; 
            return new prismworker(Object.keys(args).map(function(k){return args[k]; }),searchComponentList); 
        }
    }
    else {
        this.searchComponentList = function (filter, startingIndex, returnLimit) {
            var params = { filter: filter, detail: true };
            if (filter["type"]) {
                filter["@type"] = filter["type"];
                delete filter["type"];
            }
            if (startingIndex) params.startingIndex = startingIndex;
            if (returnLimit) params.returnLimit = returnLimit;
            var componentWorker = {}
            return $prism.api.execute("searchQuery", params).then(function(componentResult){
                componentWorker.totalNumberOfResults = componentResult.totalNumberOfResults;
                if (componentResult && componentResult.componentDetail && componentResult.componentDetail[0]) {
                    return componentResult.componentDetail.map(function ($component) {
                        var _component = { "dbid": $component["@dbid"], "type": $component["@type"] };
                        if ($component.fields && $component.fields.field) {
                            $prism_component_base.fieldDeserialize($component.fields.field, _component);
                        }
                        if ($component.fields && $component.fields.fieldMult) {
                            //$this.api.component.fieldDeserialize($component.fields.field, _component);
                        }
                        return _component;
                    });
                }
                else if (componentResult && !componentResult.componentDetail) {
                    $prism.warn("[kQuery] searchQuery returned no components.", "componentResult.totalNumberOfResults: ", JSON.stringify(componentResult.totalNumberOfResults));
                    return null;
                }
                else {
                    return undefined;
                }
            });
        }

    }

    $prism_component_base.setConnectorVirtual = function (tenant, equipmentId, virtual) {
        return $prism.api.execute("searchConfig", { query: "/*/*[@equipmentId=" + equipmentId + "]" }, tenant).then(function(searchConfigQuery){
            if (!searchConfigQuery.path) {
                throw "[kQuery] No equipment found with equipmentId \"" + equipmentId + "\"" + (tenant ? " on tenant \"" + tenant + "\"" : "");
            }
            var query = virtual === true ? "*[not(boolean(@virtual))]" : "*[boolean(@virtual)]";
            return $prism.api.execute("searchConfig", { path: searchConfigQuery.path[0], query: query }, tenant).then(function(searchConfig){
                var setConfig = { configVersion: searchConfig.configVersion };

                setConfig.update = [searchConfigQuery.path].concat(searchConfig.path).map(function (path) {
                    return { "@path": path, key: { "@id": "virtual", "#text": virtual === true ? "true" : "false" } };
                });
                return $prism.api.execute("setConfig", setConfig, tenant);
            });
        });
    }
    $prism_component_base.processConfigNode = function processNode(_node, mergeWith) {
        if (_node.node) {

            if (/^\d+$/.test(_node.node[0]["@id"])) {
                mergeWith = _node.node.map(function (__node) {
                    return processNode(__node, {});
                });

            }
            else {
                _node.node.forEach(function (__node) {
                    mergeWith[__node["@id"]] = processNode(__node, {});
                });
            }

        }
        if (_node.key) {
            return _node.key.reduce(function (p, c) {
                p[c["@id"]] = c["#text"];
                return p;
            }, mergeWith);
        }
        else {
            return mergeWith;
        }
    }
    $prism_component_base.getEquipmentConfiguration = function (equipmentId, tenant) {
        var params = { query: "/*/*[@equipmentId=" + equipmentId + "]" };
        return $prism.api.execute("searchConfig", params, tenant).then(function(searchConfigQuery){
            if (!searchConfigQuery.path) {
                $prism.error("[prism]",JSON.stringify([params,searchConfigQuery]));
                throw "[kQuery] No equipment found with equipmentId \"" + equipmentId + "\"" + (tenant ? " on tenant \"" + tenant + "\"" : "");
            }
            var equipmentConfigurationRequest = searchConfigQuery.path && $kurmi.api.execute("getConfigTree", { path: searchConfigQuery.path[0], query: "[@equipmentId=" + equipmentId + "]" }, tenant).toJson();
            var equipmentConfiguration = equipmentConfigurationRequest && equipmentConfigurationRequest.configTree && $kurmi_component_module.processConfigNode(equipmentConfigurationRequest.configTree, {});
            return equipmentConfiguration;
        });        
    }

    $prism_component_base.fieldSerialize = function (fieldObject) {
        return Object.keys(fieldObject).map(function (key) {
            return {
                "@name": key,
                "#text": fieldObject[key]
            };
        });
    }

    $prism_component_base.fieldDeserialize = function (fieldsetList, baseObject, bFavorTextValueOverTechnicalValue) {
        return fieldsetList.reduce(function (p, fieldset, i, a) {
            if (bFavorTextValueOverTechnicalValue){
                p[fieldset["@name"]] = fieldset["#text"] || fieldset["@technicalValue"];
            }
            else {
                p[fieldset["@name"]] = fieldset["@technicalValue"] || fieldset["#text"];
            }
            
            return p;
        }, baseObject || {});
    }
}

module.exports = prism_component_base;
},{"prism_worker":"prism_worker"}],"prism_logger":[function(require,module,exports){
function prism_logger($prism) {
    $prism.logParams = $prism.logParams || {};
    $prism.logParams.title = $prism.logParams.title || "prism";
    ["log","debug","error","warn","info"].forEach(function(key){
        $prism[key] = function() {
            var $arguments = arguments;
            var arrayOfArgs = Object.keys($arguments).map(function(a){ return $arguments[a];});
            if ($prism.logParams.concatWithChar) {
                arrayOfArgs = [arrayOfArgs.join($prism.logParams.concatWithChar)]
            }
            arrayOfArgs.splice(0,0,"["+$prism.logParams.title+"]");
            
            console[key].apply(null,arrayOfArgs);
        }

    })
}

module.exports = prism_logger;
},{}],"prism_sql_base":[function(require,module,exports){
const prism_sql_base = function ($prism) {
    var $prism_sql_base = this;
    
    $prism_sql_base.read = function (sqlScript) {
            return $prism.api.execute("executeReadSQL", { sqlScript: sqlScript }).then(function(sqlResult){
                return (sqlResult.sqlLineResult || []).map(row => row.item);
            });            
        }
        $prism_sql_base.getObject = function (columns, table) {
            return $prism_sql_base.read("select " + columns.join(', ') + " from " + table + ";").then(function(sqlResult){
                return sqlResult.reduce(function (p, c) {
                    var obj = columns.reduce(function (agg, colName, colIndex) { agg[colName] = c[colIndex]; return agg; }, {});
                    p.push(obj);
                    return p;
                }, []);
            });            
        } 
   
}

module.exports = prism_sql_base;
},{}],"prism_sunrise_base":[function(require,module,exports){
const prism_sunrise_base = function($prism){
    $prism_sunrise_base = this;
    
    $prism_sunrise_base.sunriseGetTenantDocuments = function (tenantName,wantedDocuments) {
        return $prism.api.execute("sunriseGetTenantDocuments",{tenant:tenantName,wantedDocument:[].concat(wantedDocuments)}).then(function(results){
            return results.document.reduce(function(res,doc){
                res[doc["@documentType"]] = JSON.parse(doc["#text"]);    
                return res;
            },{});
        });
    }       
    $prism_sunrise_base.getTenantDocument = function(tenantName){
        return $prism_sunrise_base.sunriseGetTenantDocuments(tenantName,"tenant")["tenant"];
    }
    $prism_sunrise_base.getProvisioningDocument = function(tenantName){
        return $prism_sunrise_base.sunriseGetTenantDocuments(tenantName,"provisioning")["provisioning"];
    }
    $prism_sunrise_base.getSynchroDocument = function(tenantName){
        return $prism_sunrise_base.sunriseGetTenantDocuments(tenantName,"synchro")["synchro"];
    }
    $prism_sunrise_base.releaseLockForScenario = function(scenarioDBId){
        return $prism.api.execute("releaseLockForScenario",{scenarioDBId:scenarioDBId});
    }
    $prism_sunrise_base.terminateSunriseScenario = function(scenarioDBId){
        return $prism.api.execute("releaseLockForScenario",{scenarioDBId:scenarioDBId});
    }
}

module.exports = prism_sunrise_base;
},{}],"prism_worker":[function(require,module,exports){
function prism_worker(arrayOfParams,functionToExecute,desc){
    this.msg = "im a prism worker: "+ desc;
  var $prism_worker = this;
    this.thenCounter = 0;
  try {
      var args = arguments;
      var rawResponse = functionToExecute.apply(null,arrayOfParams);
        
      if ( typeof rawResponse === 'object' && typeof rawResponse.then === 'function' && typeof rawResponse.catch === 'function'){
          //return promise
          return rawResponse;
      }
      $prism_worker.response = JSON.parse(rawResponse);
  }
  catch(e){
      $prism_worker.response = {errorMessage: e};
  }
  $prism_worker.data = $prism_worker.response;
  
  this.then = function(cb){
        this.thenCounter++;
      if (cb && $prism_worker.response.status && $prism_worker.response.status == "SUCCESS"){
          $prism_worker.data = cb($prism_worker.data);
      }
        
      return $prism_worker;
  }
  this.catch = function(ecb){
      if (ecb && (!$prism_worker.response || !$prism_worker.response.status || $prism_worker.response.status != "SUCCESS")){
          console.error("catch statement",JSON.stringify([ecb]))
          ecb($prism_worker.data);
      }
      return $prism_worker;
  }
  
}

module.exports = prism_worker;
},{}],"prism_workspace_base":[function(require,module,exports){
const prism_workspace_base = function($prism){
    var $prism_workspace_base = this;
    
    $prism_workspace_base.getList = function () {
            function flattenWorkspaceList(masterList, currentWorkspace) {
                masterList.push(currentWorkspace.name);
                (currentWorkspace.childs || []).reduce(flattenWorkspaceList, masterList);
                return masterList;
            }
            return $prism.api.execute("listWorkspaces", {}).then(function(listWorkspaceResult){
                return listWorkspaceResult && listWorkspaceResult.workspaces && JSON.parse(listWorkspaceResult.workspaces).reduce(flattenWorkspaceList, []);
            });
        },
        $prism_workspace_base.listWorkspaceFiles = function (workspaceName, fileType) {
            return $prism.api.execute("listWorkspaceFiles", { workspace: workspaceName }).then(function(listWorkspaceFilesResult){
                var files = JSON.parse(listWorkspaceFilesResult.files).map(function (file) { return file.path });
                if (fileType) {
                    var rg = typeof fileType == typeof /.*/ ? fileType : new RegExp(fileType, "i");
                    files = files.filter(function (f) { return rg.test(f); });
                }
                return files.sort();
            });
        },
        $prism_workspace_base.getWorkspaceFile = function (workspaceName, filePath) {
            return $prism.api.execute("getWorkspaceFile", { workspace: workspaceName, path: filePath }).then(function(getWorkspaceFileResult){
                return getWorkspaceFileResult;
            });           
        }
   
}

module.exports = prism_workspace_base;

},{}],"prism":[function(require,module,exports){
const prism_base = {
    version: 0,
    logParams: {}
}




function initialize(){
    var $prism = {};
    $prism.__proto__ = prism_base;
    var logger = require("prism_logger");
    require("prism_logger")($prism);
	$prism.component = new (require("prism_component_base"))($prism);
  	
    if (typeof executeKurmiAPI == "function"){
        $prism.api = new (require("prism_api_base"))($prism,executeKurmiAPI);
        $prism.api.connect();
    }
    else {
        $prism.api = new (require("prism_api_base"))($prism);
    }
    $prism.sql = new (require("prism_sql_base"))($prism);
    $prism.sunrise = new (require("prism_sunrise_base"))($prism);
    $prism.workspace = new (require("prism_workspace_base"))($prism);

    return $prism;
}

module.exports = new initialize();
},{"prism_api_base":"prism_api_base","prism_component_base":"prism_component_base","prism_logger":"prism_logger","prism_sql_base":"prism_sql_base","prism_sunrise_base":"prism_sunrise_base","prism_workspace_base":"prism_workspace_base"}]},{},[]);
