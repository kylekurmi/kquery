const prism_sql_base = function ($prism) {
    var $prism_sql_base = this;
    
    $prism_sql_base.read = function (sqlScript) {
            return $prism.api.execute("executeReadSQL", { sqlScript: sqlScript }).then(function(sqlResult){
                return (sqlResult.sqlLineResult || []).map(row => row.item);
            });            
        }
        $prism_sql_base.getObject = function (columns, table) {
            return $prism_sql_base.read("select " + columns.join(', ') + " from " + table + ";").then(function(sqlResult){
                return sqlResult.reduce(function (p, c) {
                    var obj = columns.reduce(function (agg, colName, colIndex) { agg[colName] = c[colIndex]; return agg; }, {});
                    p.push(obj);
                    return p;
                }, []);
            });            
        } 
   
}

module.exports = prism_sql_base;