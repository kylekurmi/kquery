const prism_workspace_base = function($prism){
    var $prism_workspace_base = this;
    
    $prism_workspace_base.getList = function () {
            function flattenWorkspaceList(masterList, currentWorkspace) {
                masterList.push(currentWorkspace.name);
                (currentWorkspace.childs || []).reduce(flattenWorkspaceList, masterList);
                return masterList;
            }
            return $prism.api.execute("listWorkspaces", {}).then(function(listWorkspaceResult){
                return listWorkspaceResult && listWorkspaceResult.workspaces && JSON.parse(listWorkspaceResult.workspaces).reduce(flattenWorkspaceList, []);
            });
        },
        $prism_workspace_base.listWorkspaceFiles = function (workspaceName, fileType) {
            return $prism.api.execute("listWorkspaceFiles", { workspace: workspaceName }).then(function(listWorkspaceFilesResult){
                var files = JSON.parse(listWorkspaceFilesResult.files).map(function (file) { return file.path });
                if (fileType) {
                    var rg = typeof fileType == typeof /.*/ ? fileType : new RegExp(fileType, "i");
                    files = files.filter(function (f) { return rg.test(f); });
                }
                return files.sort();
            });
        },
        $prism_workspace_base.getWorkspaceFile = function (workspaceName, filePath) {
            return $prism.api.execute("getWorkspaceFile", { workspace: workspaceName, path: filePath }).then(function(getWorkspaceFileResult){
                return getWorkspaceFileResult;
            });           
        }
   
}

module.exports = prism_workspace_base;
