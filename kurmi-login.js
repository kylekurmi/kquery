import formitem from './prism-gui-vue/FormItem.js';
const [stateError,stateUnknown,stateLoading,stateConnected] = [-1,0,1,2];

//['name','fieldType','enums','answer','multiple','modelValue']
export default {
  components: {
    formitem
  },
  props: {
    kurmiinstance: Object
  },
  data(){
    return {
      connected: stateUnknown,
      selectedTenantName: null
    }

  },
  watch:{
    selectedTenantName(val){
      if (val == "System"){
        this.kurmiinstance.api.exitSubstitution();
      }
      else {
        try {
          this.kurmiinstance.api.substitute(val);
        }
        catch (e) {
          this.connected = stateError;
          alert(e);
        }
      }
    }
  },
  computed: {
    tenants(){
      return this.kurmiinstance.component.searchComponentList({"@type":"TENANT"});
    }

  },
  methods: {
    testConnection() {
      var app = this;
      app.connected = stateLoading;
      this.kurmiinstance.api.testConnection().then(function(resp){
        app.connected = stateConnected;
      }).catch(function(e){
        app.connected = stateError;
      })
    }
  },
  template: `
  <div :title="JSON.stringify(kurmiinstance.api.config)" :class="{ 'error-state': connected == ${stateError}, 'loading-state' :connected == ${stateLoading}, 'connected-state' : connected == ${stateConnected} }">
    <formitem :name="'login'" v-model="kurmiinstance.api.config.authInfo.login" :answer="JSON.stringify(kurmiinstance.api.config.authInfo.login)"/>
    <formitem :name="'password'" :field-type="'password'" v-model="kurmiinstance.api.config.authInfo.password" :answer="JSON.stringify(kurmiinstance.api.config.authInfo.password)"/>
    <button @click="testConnection()">Login</button>
    <label>Kurmi Version <b>{{kurmiinstance.api.kurmiVersion}}</b></label>
    <formitem v-if="connected == ${stateConnected}" :name="'tenant'" v-model="selectedTenantName" :answer="JSON.stringify(selectedTenantName)" :enums="['System'].concat(tenants.map(t=>t.identifier))"/>
  </div>
  `
}
