const kurmi_component_module = function (kurmiinstance) {
    var $kurmi = kurmiinstance;
    var $kurmi_component_module = this;
    $kurmi_component_module.setConnectorVirtual = function (tenant, equipmentId, virtual) {
        return $kurmi.api.execute("searchConfig", { query: "/*/*[@equipmentId=" + equipmentId + "]" }, tenant).then(function(searchConfigQuery){
            if (!searchConfigQuery.path) {
                throw "[kQuery] No equipment found with equipmentId \"" + equipmentId + "\"" + (tenant ? " on tenant \"" + tenant + "\"" : "");
            }
            var query = virtual === true ? "*[not(boolean(@virtual))]" : "*[boolean(@virtual)]";
            return $kurmi.api.execute("searchConfig", { path: searchConfigQuery.path[0], query: query }, tenant).then(function(searchConfig){
                var setConfig = { configVersion: searchConfig.configVersion };

                setConfig.update = [searchConfigQuery.path].concat(searchConfig.path).map(function (path) {
                    return { "@path": path, key: { "@id": "virtual", "#text": virtual === true ? "true" : "false" } };
                });
                return $kurmi.api.execute("setConfig", setConfig, tenant);
            });
        });
    }
    $kurmi_component_module.processConfigNode = function processNode(_node, mergeWith) {
        if (_node.node) {

            if (/^\d+$/.test(_node.node[0]["@id"])) {
                mergeWith = _node.node.map(function (__node) {
                    return processNode(__node, {});
                });

            }
            else {
                _node.node.forEach(function (__node) {
                    mergeWith[__node["@id"]] = processNode(__node, {});
                });
            }

        }
        if (_node.key) {
            return _node.key.reduce(function (p, c) {
                p[c["@id"]] = c["#text"];
                return p;
            }, mergeWith);
        }
        else {
            return mergeWith;
        }
    }
    $kurmi_component_module.getEquipmentConfiguration = function (equipmentId, tenant) {
        return $kurmi.api.execute("searchConfig", { query: "/*/*[@equipmentId=" + equipmentId + "]" }, tenant).then(function(searchConfigQuery){
            if (!searchConfigQuery.path) {
                throw "[kQuery] No equipment found with equipmentId \"" + equipmentId + "\"" + (tenant ? " on tenant \"" + tenant + "\"" : "");
            }
            var equipmentConfigurationRequest = searchConfigQuery.path && $kurmi.api.execute("getConfigTree", { path: searchConfigQuery.path[0], query: "[@equipmentId=" + equipmentId + "]" }, tenant).toJson();
            var equipmentConfiguration = equipmentConfigurationRequest && equipmentConfigurationRequest.configTree && $kurmi_component_module.processConfigNode(equipmentConfigurationRequest.configTree, {});
            return equipmentConfiguration;
        });        
    }

    $kurmi_component_module.fieldSerialize = function (fieldObject) {
        return Object.keys(fieldObject).map(function (key) {
            return {
                "@name": key,
                "#text": fieldObject[key]
            };
        });
    }

    $kurmi_component_module.fieldDeserialize = function (fieldsetList, baseObject, bFavorTextValueOverTechnicalValue) {
        return fieldsetList.reduce(function (p, fieldset, i, a) {
            if (bFavorTextValueOverTechnicalValue){
                p[fieldset["@name"]] = fieldset["#text"] || fieldset["@technicalValue"];
            }
            else {
                p[fieldset["@name"]] = fieldset["@technicalValue"] || fieldset["#text"];
            }
            
            return p;
        }, baseObject || {});
    }
    $kurmi_component_module.searchComponentList = function (filter, startingIndex, returnLimit) {
        var params = { filter: filter, detail: true };
        if (startingIndex) params.startingIndex = startingIndex;
        if (returnLimit) params.returnLimit = returnLimit;
        var componentWorker = {}
        return $kurmi.api.execute("searchQuery", params).then(function(componentResult){
            componentWorker.totalNumberOfResults = componentResult.totalNumberOfResults;
            if (componentResult && componentResult.componentDetail && componentResult.componentDetail[0]) {
                return componentResult.componentDetail.map(function ($component) {
                    var _component = { "dbid": $component["@dbid"], "type": $component["@type"] };
                    if ($component.fields && $component.fields.field) {
                        $kurmi_component_module.fieldDeserialize($component.fields.field, _component);
                    }
                    if ($component.fields && $component.fields.fieldMult) {
                        //$this.api.component.fieldDeserialize($component.fields.field, _component);
                    }
                    return _component;
                });
            }
            else if (componentResult && !componentResult.componentDetail) {
                console.warn("[kQuery] searchQuery returned no components.", "componentResult.totalNumberOfResults: ", JSON.stringify(componentResult.totalNumberOfResults));
                return null;
            }
            else {
                return undefined;
            }
        });
    }

    $kurmi_component_module.get = function(filter) {
	    return $kurmi_component_module.searchComponentList(filter,0,1).then(function(res) { return res[0]; });
    }
};

kurmibase.prototype.modules.push(['component', kurmi_component_module]);


kQueryConstants.Component = function (equipmentId, type) {
    this.type = type;
    this.equipmentId = equipmentId;

    this.getTypeName = function () { return this.type }
    this.getFullTitle = function () { return "`${this.type} ${this.name || this.description || this.directoryNumber || this.ciscoName}`"; }
}

kQueryConstants.Filter = function (type,params) {
   this["@type"] = type;
   Object.keys(params).reduce(function(p,c){
    p[c] = params[c];
    return p;
   },this);
}
