kurmi_component_module.prototype.sdkcallKurmiAPI = function(action, requestContent, options) {
    var options = options != null ? options : {};

    var apiRequest = {
      'function': action,
      'parameters': {
        'auth': {
          'login': getCurrentTenant().DedicatedKurmiUser,
          'password': getCurrentTenant().DedicatedKurmiPassword
        }
      }
    };

    if (options.isSystem !== true) {
      apiRequest.parameters.auth.substitutionTenantOrTemplate = {
        '@type': 'TENANT',
        '@dbid': getCurrentTenant().auxiliaryId
      };
    }

    if (requestContent != null) {
      for (var contentParam in requestContent) {
        apiRequest.parameters[contentParam] = requestContent[contentParam];
      }
    }

    var curlRequest = "curl -X POST -k -Ss -w 'HTTP_STATUS_CODE:%{http_code}\\n' -H 'Content-Type: application/x-www-form-urlencoded' 'https://localhost/Kurmi/restAPI.do'"
      + " --data 'action=apiKurmi'"
      + " --data-urlencode 'jsonData=" + JSON.stringify(apiRequest).replace(/'/g, "'\\''") + "'";

    console.debug('[/connector.xml] [callKurmiAPI] Curl request: "' + JSON.stringify(curlRequest) + '".');

    try {
      var curlResultStr = sendCommand(curlRequest);
    } catch (e) {
      console.error("[/connector.xml] [callKurmiAPI] The sendCommand faced an error: " + e); // Displayed in logs, not the GUI
      throw "An error has been faced while trying to call an internal API."; // Displayed in both logs and GUI
    }

    console.debug('[/connector.xml] [callKurmiAPI] Curl result: )))' + curlResultStr + '(((');

    var curlResultParsed = /^(.*)HTTP_STATUS_CODE:(\d{3})$/.exec(curlResultStr)
    if (curlResultParsed.length > 1) {
      var httpStatus = curlResultParsed[2];

      console.debug('[/connector.xml] [callKurmiAPI] HTTP status: )))' + httpStatus + '(((');

      if (httpStatus == '200') {
        console.debug('[/connector.xml] [callKurmiAPI] JSON Result: )))' + curlResultParsed[1] + '(((');
        return JSON.parse(curlResultParsed[1]);
      } else {
        throw 'Problem: ' + curlResultStr;
      }
    } else {
      throw 'Problem: ' + curlResultStr;
    }
}