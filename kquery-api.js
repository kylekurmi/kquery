const kurmiapimodule = function(kurmiinstance) {
    this.kurmiinstance = kurmiinstance;
    this.kurmiVersion = "unknown";
}

kurmibase.prototype.modules.push(['api', kurmiapimodule]);

if (typeof executeKurmiAPI != "undefined"){
    kurmiapimodule.prototype.executeKurmiAPI = executeKurmiAPI;
}

kurmiapimodule.prototype.listTenants = function(){
    return this.kurmiinstance.component.searchComponentList({"@type":"TENANT"});
}

kurmiapimodule.prototype.getTenantDbid = function (tenantDbidOrName) {
    if (typeof tenantDbidOrName === typeof 1) {
        return tenantDbidOrName;
    } else {
        var knownDbid = localCache.get("tenantdbids", tenantDbidOrName);
        if (!knownDbid) {
            delete this.substitutionTenantOrTemplate;
            var component = this.kurmiinstance.component.get({"@type":"TENANT",field:[{"@name":"identifier","@criteria":"Equals","@technicalValue": tenantDbidOrName}]});
            localCache.store("tenantdbids", tenantDbidOrName, component["dbid"]);
            return component["dbid"];
        }
        return knownDbid;
    }
}

kurmiapimodule.prototype.substitute = function(tenantDbidOrName){
    var dbid = this.getTenantDbid(tenantDbidOrName);
    if (dbid){
        this.substitutionTenantOrTemplate = {
            "@type": "TENANT",
            "@dbid": dbid
        }
        console.info("[kQuery]", "Substituted to \"" +tenantDbidOrName+"\"");
        return true;
    }
    else {
        throw "[kQuery] "+"dbid not found";
    }
}

kurmiapimodule.prototype.exitSubstitution = function(){
    this.substitutionTenantOrTemplate = null;
    console.info("[kQuery]", "Existed substitution");
    return true;
}



kurmiapimodule.prototype.execute = function(functionName,params,optionalTenantDbidOrName){
    params = params || {};
    var $this = this;
    var kw = new kurmiworker(null);
    try {
        if (this.substitutionTenantOrTemplate){
            params.auth = params.auth || {};
            params.auth.substitutionTenantOrTemplate = kurmibase.substitutionTenantOrTemplate;
        }
        if (optionalTenantDbidOrName){
            params.auth = params.auth || {};
            params.auth.substitutionTenantOrTemplate = {"@type":"TENANT","@dbid":this.getTenantDbid(optionalTenantDbidOrName)};
        }
        
        var results = JSON.parse(this.executeKurmiAPI(functionName,params));
        kw.results = [results];
        
        var resp = kw.response;
        if (resp.status && resp.status == "SUCCESS") {
            kw.successData = resp;
        }
        if (resp.status && resp.status != "SUCCESS") {
            kw.errorData = resp;
            kw.errorMessage = resp.errorMessage;
        }
        if (resp && resp.messages) {
            kw.errorData = resp;
            kw.errorMessage = resp.messages.length > 1 ? JSON.stringify(resp.messages) : resp.messages[0].message || resp.messages[0];
        }
    }
    catch (e){
        kw.errorMessage = e;
    }

    return kw;
}

/*



kQueryBase.prototype.kurmiAPIBase = function() {
    this.api = {};
    
    this.api.getTenantDbid = function (tenantDbidOrName) {
        if (typeof tenantDbidOrName === typeof 1) {
            return tenantDbidOrName;
        } else {
            var knownDbid = localCache.get("tenantdbids", tenantDbidOrName);
            if (!knownDbid) {
                var component = this.component.get({"@type":"TENANT",field:[{"@name":"identifier","@criteria":"Equals","@technicalValue": tenantDbidOrName}]});
                localCache.store("tenantdbids", tenantDbidOrName, component["@dbid"]);
                return component["@dbid"];
            }
            return knownDbid;
        }
    }
}

*/



kurmiapimodule.prototype.searchQuery = function(params) {
    /**
 * defines the filter for the searchQuery
 * @param {Object} params
 */
    return this.executeKurmiAPI("searchQuery", params);
}

kurmiapimodule.prototype.getKurmiVersion = function() {
    return this.executeKurmiAPI("getKurmiVersion", {});
}
kurmiapimodule.prototype.executeKurmiAPI = function(functionName, params) {
    params = params || {};
    params.auth = this.kurmiinstance.config.auth;
    console.log("executeKurmiAPI", functionName, params);
    return ["executeKurmiAPI", functionName, params];
}

kurmiapimodule.prototype.checkConnection = function(path) {
    return this.kurmiinstance().config;
}

kurmiapimodule.prototype.getConfigTree = function(path) {
    return this.executeKurmiAPI("getConfigTree", {
        path: path
    });
}

