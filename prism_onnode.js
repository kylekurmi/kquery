const prism_base = require('prism_base');
const fetch = require('node-fetch');
const FormData = require('form-data');
var $prism = prism_base.init();
$prism.executeKurmiAPIasync = (new require("prism_executeKurmiAPI_async"))(fetch,FormData);
$prism.executeKurmiAPI = $prism.executeKurmiAPIasync;
module.exports = $prism;

