const prism_query = function(selector) {
    this.selector = selector;
}


const prism_base = {
    version: 0,
    logParams: {},
    moduleMap: {
        "api": function($prism) { return (new require("prism_api_base"))($prism); },
        "sql": function($prism) { return (new require("prism_sql_base"))($prism); },
        "component": function($prism) { return (new require("prism_component_base"))($prism); },
        "workspace": function($prism) { return (new require("prism_workspace_base"))($prism); },
        "sunrise": function($prism) { return (new require("prism_sunrise_base"))($prism); }
    },
    init: function() {
        var $prismparent = prism_query;
        var $prisminstance = {

        };
        $prismparent.__proto__ = $prisminstance;

        var logger = require("prism_logger");
        require("prism_logger")($prisminstance);
        
        Object.keys(this.moduleMap).forEach(function(moduleName){
            $prisminstance[moduleName] = require($prism_base.moduleMap[moduleName])($prism_base);
        });
        return $prismparent;       
    }
}

module.exports = prism_base;
