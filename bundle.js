require=(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({"prism_api_base":[function(require,module,exports){
const prismworker = require("prism_worker");

const prism_api_base = function($prism,executeKurmiAPI) {

    var $prism_api_base = this;
    var configParams = {
        auth: {
            login: "admin",
            password: "admin",
        },
        kurmiHost: null,
        apiPath: "../Kurmi/restAPI.do",
        substituteTenantOrTemplate: null
    }

    if (typeof executeKurmiAPI == "undefined"){
        var executeKurmiAPI = function (functionName, params) {
            params = params || {};
            var $this = this;
            var $promise = new Promise(function (resolve, reject) {
                try {
                    var formData = new FormData();
                    formData.append("jsonData", JSON.stringify({
                        "function": functionName,
                        "parameters": params
                    }));
                    formData.append("action", "apiKurmi");
                    fetch(configParams.apiPath, {
                        method: 'POST',
                        credentials: "include",
                        mode: 'no-cors',
                        body: formData
                    }).then(function (resp) {
                        return resp.json();
                    }).then(function (jResp) {
                        if (jResp.status && jResp.errorDetail) {
                            reject(jResp);
                        }
                        else if (jResp.status && jResp.status == "SUCCESS") {
                            resolve(jResp);
                        } else if (jResp.status && jResp.status != "SUCCESS") {
                            reject(jResp);
                        } else {
                            throw "[kQuery][unexpected json result] " + JSON.stringify(jResp);
                        }
        
                    }).catch(function (e) {
                        reject(e);
                    });
        
                } catch (e) {
                    reject({
                        status: "FAIL",
                        message: e
                    });
                }
            });
        
            return $promise;
        }
    }
    

    function getAuth() {
        return JSON.parse(JSON.stringify(configParams.auth));
    }

    function setAuth(login,password){
        configParams.auth.login = login;
        configParams.auth.password = password;
    }




    this.execute = function(functionName,params,tenantDbidOrName){
        params.auth = getAuth();
        if (tenantDbidOrName){
            params.substituteTenantOrTemplate = getSubstituteTenantOrTemplate(tenantDbidOrName);
        }
        else if (configParams.substituteTenantOrTemplate) {
            params.substituteTenantOrTemplate = substituteTenantOrTemplate;
        }
        
        
        return new prismworker([functionName,params],executeKurmiAPI);
    }


    const connectedEvents = [function(p){
        p.getTenants().then(function(res){
            p.tenants = res;
            console.debug("loaded tenants",JSON.stringify(p.tenants));
        });
        
    }];
    this.addConnectedEvent = function(callback){
        connectedEvents.push(callback);
    }

    this.isConnected = false;
    this.login = setAuth;
    this.connect = function() {
        var $this = this;
        return this.execute("getKurmiVersion",{}).then(function(res){
            connectedEvents.forEach(function(cb){
                cb($this);
            });
            console.debug("connect",true);
            $this.connected = true;
        }).catch(function(e){
            console.debug("connect",false,e);
            $this.connected = false;
        });
    }

    var tenantList = null; 
    this.tenants = tenantList;
    this.getTenants = function() {
        return $prism.component.searchComponentList({type:"TENANT"});
    }

    function getSubstituteTenantOrTemplate(tenantDbidOrName){
        var result = null;
        switch(typeof tenantDbidOrName){
            
            case typeof "tenantname": {
                try {
                    if (/SYSTEM/i.test(tenantDbidOrName)){
                        result = null;
                    }
                    //var numberDbid = tenantDbidOrName.replace(/\D/g,"");
                    //if (numberDbid && parseInt(numberDbid)){
                    //    result = getSubstituteTenantOrTemplate(parseInt(numberDbid));
                    //}
                    var tenant = $prism_api_base.tenants.filter(function(tenant){
                        tenant.identifier == tenantDbidOrName;
                    })[0];
                    result = {type: "TENANT", dbid: tenant["dbid"]};
                }
                catch(e){

                }

                break;
            }
            case typeof 1234: {

                break;
            }

        }
        console.debug("getSubstituteTenantOrTemplate",JSON.stringify([tenantDbidOrName,result]));
        return result;
    }

    this.substitute = function(tenantDbidOrName){
        configParams.substituteTenantOrTemplate = getSubstituteTenantOrTemplate(tenantDbidOrName);
        return true;
    }

    this.exitSubtitution = function() {
        configParams.substituteTenantOrTemplate = null;
        return true;
    }

}

module.exports = prism_api_base;
},{"prism_worker":"prism_worker"}],"prism_component_base":[function(require,module,exports){
const prismworker = require("prism_worker");
const prism_component_base = function($prism) {
    var $prism_component_base = this;
    if (typeof searchExistingComponent != "undefined") {
        this.searchExistingComponent = searchExistingComponent && function() { 
            var args = arguments;
            return new prismworker(Object.keys(args).map(function(k){return args[k]; }),searchExistingComponent);
            
        }
    }
    else {
        this.searchExistingComponent = function(searchFilter, startingIndex, limitedToAdminScope) { 

        }
    }
    if (typeof searchComponentList != "undefined"){
        this.searchComponentList = searchComponentList && function() { 
            var args = arguments; 
            return new prismworker(Object.keys(args).map(function(k){return args[k]; }),searchComponentList); 
        }
    }
    else {
        this.searchComponentList = function (filter, startingIndex, returnLimit) {
            var params = { filter: filter, detail: true };
            if (filter["type"]) {
                filter["@type"] = filter["type"];
                delete filter["type"];
            }
            if (startingIndex) params.startingIndex = startingIndex;
            if (returnLimit) params.returnLimit = returnLimit;
            var componentWorker = {}
            return $prism.api.execute("searchQuery", params).then(function(componentResult){
                componentWorker.totalNumberOfResults = componentResult.totalNumberOfResults;
                if (componentResult && componentResult.componentDetail && componentResult.componentDetail[0]) {
                    return componentResult.componentDetail.map(function ($component) {
                        var _component = { "dbid": $component["@dbid"], "type": $component["@type"] };
                        if ($component.fields && $component.fields.field) {
                            $prism_component_base.fieldDeserialize($component.fields.field, _component);
                        }
                        if ($component.fields && $component.fields.fieldMult) {
                            //$this.api.component.fieldDeserialize($component.fields.field, _component);
                        }
                        return _component;
                    });
                }
                else if (componentResult && !componentResult.componentDetail) {
                    console.warn("[kQuery] searchQuery returned no components.", "componentResult.totalNumberOfResults: ", JSON.stringify(componentResult.totalNumberOfResults));
                    return null;
                }
                else {
                    return undefined;
                }
            });
        }

    }

    $prism_component_base.setConnectorVirtual = function (tenant, equipmentId, virtual) {
        return $prism.api.execute("searchConfig", { query: "/*/*[@equipmentId=" + equipmentId + "]" }, tenant).then(function(searchConfigQuery){
            if (!searchConfigQuery.path) {
                throw "[kQuery] No equipment found with equipmentId \"" + equipmentId + "\"" + (tenant ? " on tenant \"" + tenant + "\"" : "");
            }
            var query = virtual === true ? "*[not(boolean(@virtual))]" : "*[boolean(@virtual)]";
            return $prism.api.execute("searchConfig", { path: searchConfigQuery.path[0], query: query }, tenant).then(function(searchConfig){
                var setConfig = { configVersion: searchConfig.configVersion };

                setConfig.update = [searchConfigQuery.path].concat(searchConfig.path).map(function (path) {
                    return { "@path": path, key: { "@id": "virtual", "#text": virtual === true ? "true" : "false" } };
                });
                return $prism.api.execute("setConfig", setConfig, tenant);
            });
        });
    }
    $prism_component_base.processConfigNode = function processNode(_node, mergeWith) {
        if (_node.node) {

            if (/^\d+$/.test(_node.node[0]["@id"])) {
                mergeWith = _node.node.map(function (__node) {
                    return processNode(__node, {});
                });

            }
            else {
                _node.node.forEach(function (__node) {
                    mergeWith[__node["@id"]] = processNode(__node, {});
                });
            }

        }
        if (_node.key) {
            return _node.key.reduce(function (p, c) {
                p[c["@id"]] = c["#text"];
                return p;
            }, mergeWith);
        }
        else {
            return mergeWith;
        }
    }
    $prism_component_base.getEquipmentConfiguration = function (equipmentId, tenant) {
        var params = { query: "/*/*[@equipmentId=" + equipmentId + "]" };
        return $prism.api.execute("searchConfig", params, tenant).then(function(searchConfigQuery){
            if (!searchConfigQuery.path) {
                console.error("[prism]",JSON.stringify([params,searchConfigQuery]));
                throw "[kQuery] No equipment found with equipmentId \"" + equipmentId + "\"" + (tenant ? " on tenant \"" + tenant + "\"" : "");
            }
            var equipmentConfigurationRequest = searchConfigQuery.path && $kurmi.api.execute("getConfigTree", { path: searchConfigQuery.path[0], query: "[@equipmentId=" + equipmentId + "]" }, tenant).toJson();
            var equipmentConfiguration = equipmentConfigurationRequest && equipmentConfigurationRequest.configTree && $kurmi_component_module.processConfigNode(equipmentConfigurationRequest.configTree, {});
            return equipmentConfiguration;
        });        
    }

    $prism_component_base.fieldSerialize = function (fieldObject) {
        return Object.keys(fieldObject).map(function (key) {
            return {
                "@name": key,
                "#text": fieldObject[key]
            };
        });
    }

    $prism_component_base.fieldDeserialize = function (fieldsetList, baseObject, bFavorTextValueOverTechnicalValue) {
        return fieldsetList.reduce(function (p, fieldset, i, a) {
            if (bFavorTextValueOverTechnicalValue){
                p[fieldset["@name"]] = fieldset["#text"] || fieldset["@technicalValue"];
            }
            else {
                p[fieldset["@name"]] = fieldset["@technicalValue"] || fieldset["#text"];
            }
            
            return p;
        }, baseObject || {});
    }
}

module.exports = prism_component_base;
},{"prism_worker":"prism_worker"}],"prism_worker":[function(require,module,exports){

function prism_worker(arrayOfParams,functionToExecute){
    var $this = this;
    try {
        var args = arguments;
        var rawResponse = functionToExecute.apply({},arrayOfParams);
        if (typeof rawResponse == "object"){
            return rawResponse;
        }
        var response = JSON.parse(rawResponse);
    }
    catch(e){
        var response = {errorMessage: e};
    }
    this.response = response;
    
    this.then = function(cb){
        if (response.status && response.status == "SUCCESS"){
            response = cb(response);
        }
        return $this;
    }
    this.catch = function(ecb){
        if (!response || !response.status || response.status != "SUCCESS"){
            ecb(response);
        }
        return $this;
    }

}

module.exports = prism_worker;
},{}],"prism":[function(require,module,exports){
const prism_base = {
    version: 0

}

function initialize(){
    var $prism = {}
    $prism.__proto__ = prism_base;
	$prism.component = new (require("prism_component_base"))($prism);
  	
    if (typeof executeKurmiAPI == "function"){
        $prism.api = new (require("prism_api_base"))($prism,executeKurmiAPI);
        $prism.api.connect();
    }
    else {
        $prism.api = new (require("prism_api_base"))($prism);
    }
    return $prism;
}

module.exports = new initialize();
},{"prism_api_base":"prism_api_base","prism_component_base":"prism_component_base"}]},{},[]);
