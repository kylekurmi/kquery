
function remoteKurmiAPI(configOptions){
    kurmiAPIBase.call(this);
    this.api.config = {
        restAPIpath: "../../Kurmi/restAPI.do",
        authInfo: {
            login: null,
            password: null
        }
    };

    Object.keys(configOptions || {}).forEach(function (key) {
        this.api.config[key] = configOptions[key] || localCache.get(key) || this.api.config[key];
    });

    this.getAuth = function () {
        return JSON.parse(JSON.stringify(this.api.config.authInfo));
    }
    
    this.execute = function (functionName, params, tenantDbidOrName, isAsync) {
        params.auth = $this.getAuth();
        if (tenantDbidOrName) {
            this.api.getTenantDbid(tenantDbidOrName)
            params.auth.substitutionTenantOrTemplate = {
                "@type": "TENANT",
                "@dbid": $this.getTenantDbid(tenantDbidOrName)
            };
        }
        return new kQueryAPIWorker(functionName, params, isAsync);
    }
    this.api.executeAsync = function (functionName, params, tenantDbidOrName) {
        return this.api.execute(functionName, params, tenantDbidOrName, true);
    };

    this.api.testConnection = function () {
        
        this.api.execute("getKurmiVersion", {}).then(function (d) {
            this.api.isConnected = true;
                console.debug("[kQuery]", "Connection Successful", "Version: " + d.kurmiVersion);
            }).fail(function (e) {
                this.api.isConnected = false;
                console.error("[kQuery]", "Not Connected");
            });
        
    }
    function initialize() {
        
        if (!this.api.config.authInfo.login) {
            this.api.config.authInfo.login = localCache.get("config.authInfo", "login") || prompt("KURMI API login", "admin");
            localCache.store("config.authInfo", "login", this.api.config.authInfo.login);
        }
        if (!this.api.config.authInfo.password) {
            this.api.config.authInfo.password = localCache.get("config.authInfo", "password") || prompt("KURMI API password", "admin");
            localCache.store("config.authInfo", "password", this.api.config.authInfo.password);
        }

        this.api.testConnection();

        //$this.config.authInfo.login = localCache.get("authInfo","login");
        //$this.config.authInfo.password = localCache.get("authInfo","password");
    }
}

kQueryModules["api"] = function(){
    var $this = this;
    var api = {
        config : {
            restAPIpath: "../../Kurmi/restAPI.do",
            authInfo: {
                login: null,
                password: null
            }
        },
        getTenantDbid : function(tenantNameOrDbid) {
            return tenantNameOrDbid;
        },
        getAuth : function() {
            return api.config.authInfo;
        }
    }
    return api;
}


kQueryBase.prototype.execute = function(methodName,params,optionalTenantNameOrDbid){
    var $this = this;
    params.auth = this.api.getAuth();
    if (tenantDbidOrName) {
        this.api.getTenantDbid(optionalTenantNameOrDbid)
        params.auth.substitutionTenantOrTemplate = {
            "@type": "TENANT",
            "@dbid": this.api.getTenantDbid(optionalTenantNameOrDbid)
        };
    }
    return new kQueryAPIWorker(functionName, params);
}