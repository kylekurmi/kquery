kurmiapimodule.prototype.loadApiMethods = async function(){
    var kurmiinstance = this.kurmiinstance;
    var $kurmiapimodule = this;

    $kurmiapimodule.apiMethods = {};
    $kurmiapimodule.apiMethodNames = [];
    $kurmiapimodule.wsdlStorage = {};
    //$kurmiapimodule.wsdlStorage = this.config.wsdlStorage;

    function parseWsdl(data) {
        let doc = document.implementation.createHTMLDocument("Kurmi WSDL Document");
        doc.write(data);
        $kurmiapimodule.wsdlStorage["main"] = data;
        localCache.store("wsdl", "main", data);
        doc.querySelectorAll('message').forEach(function (item, i) {
            kurmiinstance.api.apiMethodNames.push(item.getAttribute('name'));
            var name = item.getAttribute('name');
            /*
            $kurmiapimodule.apiMethods[name] = function (params, tenantName) {
                return $kurmiapimodule.execute(name, params || {}, tenantName).toJSON();
            }

            $kurmiapimodule.apiMethods[name].element = (item.querySelector('part').getAttribute('element') || ":").split(':')[1];
            $kurmiapimodule.apiMethods[name].getParams = function () {
                    return $kurmiapimodule.getParamsForAPIMethod(name);
                }
            $kurmiapimodule.apiMethods[name].execute = function (params, tenantName) {
                return $kurmiapimodule.execute(name, params || {}, tenantName).toJSON();
            }
            */
            $kurmiapimodule[name] = function (params, tenantName) {
                return $kurmiapimodule.execute(name, params || {}, tenantName);
            }

            $kurmiapimodule[name].element = (item.querySelector('part').getAttribute('element') || ":").split(':')[1];
            $kurmiapimodule[name].getParams = function () {
                return getParamsForAPIMethod(name, $kurmiapimodule[name].element);
            }
            $kurmiapimodule[name].getPrototype = function () {
                return getFunctionForParams(name, $kurmiapimodule[name].element);
            }

                getFunctionForParams
            $kurmiapimodule[name].execute = function (params, tenantName) {
                return $kurmiapimodule.execute(name, params || {}, tenantName);
            }
        });
        console.log("Loaded API Methods. kQuery is ready!");
        $kurmiapimodule.ready = true;
    };

    function wsdlResolver(wsdl, queryselector, objectIn) {

        (wsdl).querySelectorAll(queryselector).forEach(function (i, item) {
            jQuery(item).querySelectorAll('xs\\:element').forEach(function (iitem, ii) {
                var name = (iitem).getAttribute('name');
                var type = (iitem).getAttribute('type');
                var objectToPass = {};
                if (objectIn[name] && typeof objectIn[name] === typeof {}) {
                    objectIn[name] = [objectIn[name]];
                }
                if (objectIn[name] && typeof objectIn[name] === typeof []) {
                    objectToPass = {};
                    objectIn[name][objectIn[name].length] = objectToPass;

                } else {
                    objectIn[name] = {};
                    objectToPass = objectIn[name];
                }
                $kurmiapimodule.wsdlResolver(wsdl, `xs\\:complexType[name="${type}"]`, objectToPass);
            });
        });


    }

    async function getParamsForAPIMethod(methodName,elementName) {
        methodName = ($kurmiapimodule.apiMethods[methodName] || {}).element || methodName;
        if (!$kurmiapimodule.wsdlStorage["main"]) {
            $kurmiapimodule.loadApiMethods();
        }
        let doc = document.implementation.createHTMLDocument("New Document");
        doc.write($kurmiapimodule.wsdlStorage["main"])
        var schemaLocation = doc.querySelector('types [namespace=\"http://www.nates.fr/API/1.0\"]').getAttribute('schemaLocation');
        var params = [];
        var data = await (await (fetch(schemaLocation))).text();
        let doc2 = document.implementation.createHTMLDocument("New Document");
        doc2.write(data);
        console.log(doc2.querySelector(`xs\\:element[name="${elementName}"]`).outerHTML);
        doc2.querySelectorAll(`xs\\:element[name="${elementName}"]`).forEach(function (item, i) {
            item.querySelectorAll('xs\\:element').forEach(function (iitem, ii) {
                var name = (iitem).getAttribute('name');
                var xmltype = ((iitem).getAttribute('type') || "").replace("tns:", "");
                if (xmltype) {
                    //var typp = (doc2).querySelector(`xs\\:complexType[name="${xmltype}"]`);
                    //console.log("type", (typp).outerHTML);
                    params.push((iitem).getAttribute('name'));
                }
                else {
                    params.push((iitem).getAttribute('name'));
                }
            });
            item.querySelectorAll('xs\\:extension').forEach(function (iitem, ii) {
                var base = (iitem).getAttribute('base').split(':')[1];
                params.push(base);
                console.log(doc2.querySelector(`xs\\:extension[base="${(iitem).getAttribute('base')}"]`).outerHTML);
            });
        });
        console.log(params);
        return params;
    }
    this.wsdlTypes = {};
    var wsdlTypes = this.wsdlTypes;

    function processParam(item){
        var name = item.getAttribute('name');
        var minOccurs = parseInt(item.getAttribute('minOccurs')|| "-1");
        var prefix = "o";
        switch(item.getAttribute('type')){
            case "xs:string": {
                prefix = "s";
                break;
            }
            case "xs:boolean":{
                prefix = "b";
                break;
            }
            case "xs:int": {
                prefix = "i";
                break;
            }
                        
        }
        var paramName = prefix + name.replace(/^(.)/,function(r,fm) { return fm.toUpperCase();})
        paramName = minOccurs == 0 ? "optional_" + paramName : paramName;
        var functionLogic = minOccurs == 0 ? `\tif (${paramName}) this["${paramName}"] = ${paramName};` : `\tthis["${paramName}"] = ${paramName};`;
        paramName = paramName.replace(/^(.)/,function(r,fm) { return fm.toLowerCase();});
        return [paramName,functionLogic];
    }

    async function getFunctionForParams(methodName,elementName) {
        methodName = ($kurmiapimodule.apiMethods[methodName] || {}).element || methodName;
        if (!$kurmiapimodule.wsdlStorage["main"]) {
            $kurmiapimodule.loadApiMethods();
        }
        let doc = document.implementation.createHTMLDocument("New Document");
        doc.write($kurmiapimodule.wsdlStorage["main"])
        var schemaLocation = doc.querySelector('types [namespace=\"http://www.nates.fr/API/1.0\"]').getAttribute('schemaLocation');
        var params = [];
        var functionLines = [];
        var data = await (await (fetch(schemaLocation))).text();
        let doc2 = document.implementation.createHTMLDocument("New Document");
        doc2.write(data);
        console.log(doc2.querySelector(`xs\\:element[name="${elementName}"]`).outerHTML);


        doc2.querySelectorAll(`xs\\:element[name="${elementName}"]`).forEach(function (item, i) {
            
            item.querySelectorAll('xs\\:element').forEach(function (iitem, ii) {
                var [paramName,functionLogic] = processParam(iitem);
                params.push(paramName);
                functionLines.push(functionLines);
            });
            item.querySelectorAll('xs\\:extension').forEach(function (iitem, ii) {
                var base = (iitem).getAttribute('base').split(':')[1];
                
                doc2.querySelectorAll(`xs\\:complexType[name="${base}"] xs\\:element`).forEach(function(iiitem){
                    var [paramName,functionLogic] = processParam(iiitem);
                    params.push(paramName);
                    functionLines.push(functionLines);
                });
                                
            });
        });
        console.log(params);

        return new Function(params, functionLines.join('\r\n'));
        //return params;
    }

    var wsdl = localCache.get("wsdl", "main");
    if (wsdl) {
        return parseWsdl(wsdl);
    }


    return fetch($kurmiapimodule.config.restAPIpath.replace("restAPI.do","APIService?wsdl")).then(function (data) {
        return data.text();
    }).then(parseWsdl).catch(function (e) {
        console.error("Failed to load SOAP API METHODS!");
        $kurmiapimodule.ready = false;
    });


}
