const kurmi_sql_module = function (kurmiinstance) {
    var $this = kurmiinstance;
    
        this.read = function (sqlScript) {
            return $this.api.execute("executeReadSQL", { sqlScript: sqlScript }).then(function(sqlResult){
                return (sqlResult.sqlLineResult || []).map(row => row.item);
            });            
        }
        this.getObject = function (columns, table) {
            return this.read("select " + columns.join(', ') + " from " + table + ";").then(function(sqlResult){
                return sqlResult.reduce(function (p, c) {
                    var obj = columns.reduce(function (agg, colName, colIndex) { agg[colName] = c[colIndex]; return agg; }, {});
                    p.push(obj);
                    return p;
                }, []);
            });            
        } 
   
}

kurmibase.prototype.modules.push(["sql",kurmi_sql_module]);
