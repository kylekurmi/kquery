const kurmi_workspace_module = function(kurmiinstance){
    var $this = kurmiinstance;
    
        this.getList = function () {
            function flattenWorkspaceList(masterList, currentWorkspace) {
                masterList.push(currentWorkspace.name);
                (currentWorkspace.childs || []).reduce(flattenWorkspaceList, masterList);
                return masterList;
            }
            return $this.api.execute("listWorkspaces", {}).then(function(listWorkspaceResult){
                return listWorkspaceResult && listWorkspaceResult.workspaces && JSON.parse(listWorkspaceResult.workspaces).reduce(flattenWorkspaceList, []);
            });
        },
        this.listWorkspaceFiles = function (workspaceName, fileType) {
            return $this.api.execute("listWorkspaceFiles", { workspace: workspaceName }).then(function(listWorkspaceFilesResult){
                var files = JSON.parse(listWorkspaceFilesResult.files).map(function (file) { return file.path });
                if (fileType) {
                    var rg = typeof fileType == typeof /.*/ ? fileType : new RegExp(fileType, "i");
                    files = files.filter(function (f) { return rg.test(f); });
                }
                return files.sort();
            });
        },
        this.getWorkspaceFile = function (workspaceName, filePath) {
            return $this.execute("getWorkspaceFile", { workspace: workspaceName, path: filePath }).then(function(getWorkspaceFileResult){
                return getWorkspaceFileResult;
            });           
        }
   
}


kurmibase.prototype.modules.push(["workspace",kurmi_workspace_module]);
