function kQueryAPIWorker(functionName, params, isAsync) {
    var $kQueryAPIWorker = this;
    $kQueryAPIWorker.successData = null;
    $kQueryAPIWorker.errorData = null;
    $kQueryAPIWorker.errorMessage = null;

    if (isAsync && _kqueryType == KQUERY_TYPES.KQUERY_WEB) {
        var $promise = new Promise(function (resolve, reject) {
            try {
                var formData = new FormData();
                formData.append("jsonData", JSON.stringify({
                    "function": functionName,
                    "parameters": params
                }));
                formData.append("action", "apiKurmi");
                fetch($this.config.restAPIpath, {
                    method: 'POST',
                    credentials: "include",
                    body: formData
                }).then(function (resp) {
                    return resp.json();
                }).then(function (jResp) {
                    if (jResp.status && jResp.status == "SUCCESS") {
                        resolve(jResp);
                    } else if (jResp.status && jResp.status != "SUCCESS") {
                        reject(jResp);
                    } else {
                        throw "[kQuery][unexpected json result] " + JSON.stringify(jResp);
                    }

                }).catch(function (e) {
                    reject(e);
                });

            } catch (e) {
                reject({
                    status: "FAIL",
                    message: e
                });
            }
        });

        return $promise;
    }

    else {
        try {
            if (!isAsync && _kqueryType == KQUERY_TYPES.KQUERY_WEB) {
                var xhttp = new XMLHttpRequest();
                xhttp.open("POST", $this.config.restAPIpath, false);
                xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                var formData = new FormData();
                formData.append("jsonData", JSON.stringify({
                    "function": functionName,
                    "parameters": params
                }));
                formData.append("action", "apiKurmi");
                xhttp.send(new URLSearchParams(formData).toString());
                var resp = JSON.parse(xhttp.responseText);
            }
            else {
                var resp = JSON.parse($this.api.executeKurmiAPI(functionName, params));
            }
            if (resp.status && resp.status == "SUCCESS") {
                $kQueryAPIWorker.successData = resp;
            }
            if (resp.status && resp.status != "SUCCESS") {
                $kQueryAPIWorker.errorData = resp;
                $kQueryAPIWorker.errorMessage = resp.errorMessage;
            }
            if (resp && resp.messages) {
                $kQueryAPIWorker.errorData = resp;
                $kQueryAPIWorker.errorMessage = resp.messages.length > 1 ? JSON.stringify(resp.messages) : resp.messages[0].message || resp.messages[0];
            }
        } catch (e) {
            $kQueryAPIWorker.errorMessage = e;
            $kQueryAPIWorker.errorData = {
                status: "FAIL",
                message: e
            };
        }
        $kQueryAPIWorker.then = function (cb) {
            if (cb && $kQueryAPIWorker.successData) {
                cb($kQueryAPIWorker.successData);
            }
            return $kQueryAPIWorker;
        }
        $kQueryAPIWorker.fail = function (cb) {
            if (cb && $kQueryAPIWorker.errorData) {
                cb($kQueryAPIWorker.errorData);
            }
            return $kQueryAPIWorker;
        }
        $kQueryAPIWorker.next = $kQueryAPIWorker.then;
        $kQueryAPIWorker.error = $kQueryAPIWorker.fail;
        $kQueryAPIWorker.toJSON = function () {
            if ($kQueryAPIWorker.errorData) {
                throw JSON.stringify($kQueryAPIWorker.errorData);
            }
            return $kQueryAPIWorker.successData;
        }
        $kQueryAPIWorker.data = $kQueryAPIWorker.toJSON
        $kQueryAPIWorker.response = $kQueryAPIWorker.toJSON
        return $kQueryAPIWorker;

    }

}