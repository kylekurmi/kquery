const KQUERY_TYPES = {
    KQUERY_WEB: 0,
    KQUERY_SUNRISE: 1,
    KQUERY_KUP: 2,
    KQUERY_MAINTENANCE_QF: 3
};


var _localCache = {};

var localCache = {
    get: function (table, key) {
        if (localStorage) {
            localStorage["kQuery"] = localStorage["kQuery"] || "{}";
            var kquerystorage = JSON.parse(localStorage["kQuery"]);
            var _table = kquerystorage;
        } else {
            var _table = (_localCache || {});
        }

        table.split('.').forEach(function (pathSplit, pathSplitI) {
            _table = _table[pathSplit] || {};
        });

        if (key) {
            return _table[key];
        }
        return _table;
    },
    store: function (table, key, value) {
        if (localStorage) {
            localStorage["kQuery"] = localStorage["kQuery"] || "{}";
            var kquerystorage = JSON.parse(localStorage["kQuery"]);

            var _table = kquerystorage;
            table.split('.').forEach(function (pathSplit, pathSplitI) {
                if (!_table[pathSplit]) {
                    _table[pathSplit] = {};
                }
                _table = _table[pathSplit];
            });
            _table[key] = value;

            localStorage["kQuery"] = JSON.stringify(kquerystorage);
        } else {
            _localCache[table] = _localCache[table] || {};
            _localCache[table][key] = value;
        }
    }
}


var kQueryConstants = {};
kQueryConstants.logHeader = "[kQuery]";
kQueryConstants.auditType = {
    None: "None",
    New: "New",
    Optimized: "Optimized",
    Core: "Core",
    Full: "Full",
    Inference: "Inference"
};
kQueryConstants.auditSync = {
    None: "None",
    DB: "DB",
    System: "System"
};
kQueryConstants.criteria = {
    simpleString: {
        StartsWith: "^=",
        Contains: "~=",
        EndsWith: "$=",
        Equals: "=",
        DoesntStartWith: "!^=",
        DoesntContains: "!~=",
        DoesntEndWith: "!$=",
        DiffersFrom: "!="
    },
    numericFields: {
        Equal: "=",
        NotEqual: "!=",
        LowerThan: "<",
        LowerEqualThan: "<=",
        GreaterThan: ">",
        GreaterEqualThan: "<",
        Set: "[]",
        NotSet: "![]"
    },
    datetimeFields: {
        Before: "<",
        After: ">",
        Exact: "="
    },
    multivalueFields: {
        ConditionContains: "*=",
        ConditionDoesntContains: "!*="
    }
};


const kurmibase = function() {
    this.initDate = null
    this.version = 1;
    
}
kurmibase.prototype.modules = [];
kurmibase.prototype.loadModule = function(name,$module){
    this[name] = new $module(this);
    this[name].kurmiinstance = this;
    
}
kurmibase.prototype.loadModules = function() {
    var $this = this;
    this.modules.forEach(function([name,$module]) {
        $this.loadModule(name,$module);        
    });
}

function _kurmiworker(){

}

function kurmiworker(selector,results){
    var _results = results; 
    this.get = function(num) {
        return _results[num-1];
    }

    this.then = function (cb) {
        if (!this.errorMessage && this.successData) {
            var mutation = cb(this.successData);
            if (mutation) {
                this.successData = mutation;
            }
        }
        if (!this.errorMessage && !this.successData) {
            var mutation = cb(this.target);
            if (mutation) {
                this.target = mutation;
            }
        }
        return this;
    }

    this.catch = function (cb) {
        this.errorMessage && cb(this.errorMessage);
        return this;

    }

    this.toJson = function () {
        try {
            if (this.errorData){
                //$this.logger.error(JSON.stringify(this.errorData));
                throw this.errorData.errorDetail || this.errorData.messages[0];
            }
            var result = this.successData || this.target || {};
            if (typeof result != "object") {
                return JSON.parse(result);
            } else {
                return result;
            }
        }
        catch (e) {
            
            throw e;
        }

    }
    this.toJSON = this.toJson;
    this.toJsonString = function () {
        var result = this.successData || this.target || {};
        return JSON.stringify(result);
    }

    if (typeof selector == typeof kQueryConstants.Filter){

    }
    
}


function loadKurmi() {

    const kurmi = function(selector) {
        kurmi.selector = selector;
        kurmi.version++;
        if (selector["@type"]){
            return new kurmiworker(selector);
        }

        
    }

    kurmi.__proto__ = new kurmibase(kurmi);
    kurmi.initDate = new Date();
    kurmi.loadModules();
    return kurmi;
}

