const prism_base = require('prism_base');
var $prism = prism_base.init();
$prism.executeKurmiAPIasync = (new require("prism_executeKurmiAPI_async"))(fetch,FormData);
$prism.executeKurmiAPIsync = require("prism_executeKurmiAPI_browsersync");
$prism.executeKurmiAPI = $prism.executeKurmiAPIasync;
module.exports = $prism;
